# Situs 

<h2 style="text-align: center;">a hypertext editor for independent authors</h2>
Situs is an in-browser web authoring framework for independent authors.

## Hypertext
Hypertext is ('encursive')[/define/encursive], runs/writes into/within itself.

The codebase is built with 100% web-native standards that avoid the need for a build step:
  * native `.html`, _hypertext_, not JSON APIs as the _medium_ of communication
  * native `.css` (now has nesting selectors) instead of a preprocessor
  * native `.js` with JSdoc annotations and linting via  `// @ts-check` for type-_sanity_

Because the entire codebase is web-native, source files are immediately ready for release,
are human _readable_, thus decipherable and _hackable_. Exercise your right to repair and mod.

## "Site Generator"
Because Situs makes web authoring simple, some would call this a "_static_ site generator",
but we disagree with this and prefer to call it rather just a **"site generator"**, because:
  1. Situs provides a mutable container, a _site_ for publishing _dynamic_ web content.
    A. Situs does not watch a source, having to re-_build_ a static repository for release.
    B. Situs provides a web-native environment for reading/writing to a dynamic source:
      * an IPNS name identifying mutable IPFS content addressing the site's source.

## Development Setup:
Because Situs is web-native simply _serve_ the `public` directory, however you wish.

The recommendation is to:
 1. Install IPFS Desktop; Enable Brave IPFS node via `brave://ipfs`
 2. Ensure that Node installed and `http-server` is available globally on the commandline.
 3. Download or pull the repo.
 4. From the root of the repo, evoke the scripts defined in package.json:
   - `npm run dev`
   - `npm run release`

## Minimal Setup:
