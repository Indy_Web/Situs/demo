// @ts-check

const enableDebug = true
const debug = enableDebug
    ? console.log
    : () => { return; }

//import { EditableElement } from './index.mjs';


const style = /*css*/
`editor-toolbar {
    position: absolute;
    transform: translate3d(-50%, -100%, 0);
    transition: top 0.3s ease-in-out, left 0.3s ease-in-out;
    font-family: "Sono Variable", sans-serif;
    color: #ffedd5;
    
    & menu {
        display: flex;
        list-style-type: none;
        padding-inline-start: 0;
        min-width: 2em;
        min-height: 2em;
        border-radius: 3px;
        user-select: none;
        cursor: pointer;

        & li {
            display: grid;
            place-content: center;
            gap: 0.25em;
            width: 2em;
            height: 2em;
            text-align: center;

            &> * {
                width: 1.5em;
                height: 1.5em;
            }
        }
    }

    &> menu {
        flex-direction: row;
        background: #2f1e08;
        position: relative;
        
        &> li {
            position: relative;

            & menu.dropdown {
                position: absolute;
                margin-block: 0;
                top: 100%;
                left: 0;
                flex-direction: column;
                visibility: hidden;

                & li {
                    background: #6f6150;
                }

                &> li {
                    position: relative;
                    & menu.drawer {
                        position: absolute;
                        top: 0;
                        left: 100%;
                        margin-block-start: 0;
                        flex-direction: row;
                        visibility: hidden;
                        height: 2em;

                        &> li> span {
                            height: 100%;
                        }
                    }
                }
            }

            & :is(svg:hover, span:hover) {
                color: red;
            }
        }

        &::before {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            transform: translate3d(-50%, 0, 0);
            border-style: solid;
            border-width: 6px 6px 0;
            border-color: #2f1e07 transparent transparent;
        }
    }
}`


export class EditorToolbar extends HTMLElement {
    /** @type {boolean} */
    isOpen;

    /**
     * The <editor-toolbar> element:
     * @type {HTMLElement} & Partial<EditableElement> */
    #editable;

    /** @type {HTMLElement|null} */
    #target;

    /** @type {HTMLMenuElement} */
    #menu;

    /** @type {HTMLMenuElement} */
    #alignmentMenu

    /** @type {HTMLMenuElement} */
    #blocktextMenu

    //? /** @type {HTMLMenuElement} */
    //? #headerSubMenu

    /** @type {string} */
    #direction

    constructor() {
        super();

        this.#target = null;
        this.#editable = /** @type {HTMLElement} */ //& EditableElement
            ((/** @type {ShadowRoot} */ (this.getRootNode())).host)
        this.isOpen = false;
        this.innerHTML = /* html */ `
        <style>${style}</style>
        <menu class="main">
            <li id="blocktext-menu">
                <element-icon></element-icon>
                <menu class="dropdown">
                    <li title="header" id="blocktext-header">
                        <span>#</span>
                        <menu class="drawer">
                            <li title="<h1>" id="h1"><span>1</span></li>
                            <li title="<h2>" id="h2"><span>2</span></li>
                            <li title="<h3>" id="h3"><span>3</span></li>
                            <li title="<h4>" id="h4"><span>4</span></li>
                            <li title="<h5>" id="h5"><span>5</span></li>
                            <li title="<h6>" id="h6"><span>6</span></li>
                        </menu>
                    </li>
                    <li title="paragraph" id="blocktext-paragraph"><span>¶</span></li>
                    <li title="blockquote" id="blocktext-quote"><span>“”</span></li>
                    <!--<li title="icon"><dynamic-icon src="/icons/dynamic-icon.svg" fill="#ffedd5"/></li>-->
                </menu>
            </li>
            <li id="style-menu">
                <style-icon></style-icon>
                <menu class="dropdown">
                    <li title="bold" id="style-bold"><span style="font-weight: 700;">B</span></li>
                    <li title="italic" id="style-italic"><span style="font-style: italic;">i</span></li>
                    <li title="underline" id="style-underline"><span style="text-decoration: underline;">U</span></li>
                    <li title="strikethrough" id="style-strikethrough"><span style="text-decoration: line-through;">S</span></li>
                </menu>
            </li>
            <li id="alignment-menu">
                <align-icon></align-icon>
                <menu class="dropdown">
                    <li id="align-left">
                        <align-left-icon/>
                    </li>
                    <li id="align-center">
                        <align-center-icon/>
                    </li>
                    <li id="align-right">
                        <align-right-icon/>
                    </li>
                    <li id="align-justify">
                        <align-justify-icon/>
                    </li>
                    <li id="align-left-justify">
                        <align-left-justify-icon/>
                    </li>
                    <li id="align-center-justify">
                        <align-center-justify-icon/>
                    </li>
                    <li id="align-right-justify">
                        <align-right-justify-icon/>
                    </li>
                </menu>
            </li>
        </menu>
        `
        this.#direction = document.querySelector('html')?.dir || 'ltr'

        this.#menu = /** @type {HTMLMenuElement} */
            (this.querySelector('menu'))
        this.#alignmentMenu = /** @type {HTMLMenuElement} */
            (this.#menu.querySelector('#alignment-menu'))
        this.#blocktextMenu = /** @type {HTMLMenuElement} */
            (this.#menu.querySelector('#blocktext-menu'))

        //! this.style.visibility = 'hidden'
        this.style.display = 'none'

        this.addEventListener('click', (e) => this.#handleClick(e))

        console.dir(this)
    }

    /** @param {HTMLElement|Range} $  */
    openMenu($) {
        if (!$) return;

        const { top, left, width } = $.getBoundingClientRect()
        this.style.top = `${top + (document.scrollingElement?.scrollTop || 0)}px`
        this.style.left = `${Math.floor(left + (width / 2))}px`
        this.style.display = ''

        if ($ instanceof HTMLElement) {
            this.#alignmentMenu.style.display = (mayBeAligned($)) ? '' : 'none'
            this.#blocktextMenu.style.display = (isBlocktextElement($)) ? '' : 'none'
            this.#target = $
            this.isOpen = true
        }
        if ($ instanceof Range) {
            this.#alignmentMenu.style.display = 'none'
            this.#blocktextMenu.style.display = 'none'
        }
        window.requestAnimationFrame(() => this.style.transition = '')
    }

    closeMenu() {
        if (!this.isOpen) return;
        Array.from(this.#menu.children).forEach($child => {
            const $dropdown = (/** @type {HTMLElement} */
                ($child.querySelector('menu.dropdown')))
            if ($dropdown) $dropdown.style.visibility = 'hidden'
            const $drawer = (/** @type {HTMLElement} */
                ($child.querySelector('menu.drawer')))
            if ($drawer) $drawer.style.visibility = 'hidden'
        })
        this.style.display = 'none'
        this.style.transition = 'none'
        this.isOpen = false
        this.#target = null
    }

    /** @param {MouseEvent} e */
    #handleClick(e) {
        e.stopPropagation()

        const $clicked = (/** @type {HTMLElement} */
            (e.target)).closest('li')
        if (!$clicked) return;

        //? Clicked on main menu or submenu item?
        if ($clicked.closest('menu')?.classList.contains('main')) {
            Array.from(this.#menu.children).forEach($child => {
                const $dropdown = (/** @type {HTMLElement} */ ($child.querySelector('menu.dropdown')))
                const $drawer = (/** @type {HTMLElement} */ ($child.querySelector('menu.drawer')))
                if (!$dropdown) return;
                if ($clicked === $child) {
                    $dropdown.style.visibility = ($dropdown.style.visibility === 'visible')
                        ? 'hidden' : 'visible'
                }
                else {
                    $dropdown.style.visibility = 'hidden'
                    if ($drawer) $drawer.style.visibility = 'hidden'
                }
            })
        }
        else {
            if ($clicked.id.startsWith('blocktext-'))
                this.setBlocktextElement($clicked.id)
            if ($clicked.id.startsWith('style-'))
                this.setStyle($clicked.id)
            if ($clicked.id.startsWith('align-'))
                this.setAlignment($clicked.id)
            if (['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].includes($clicked.id))
                this.replaceTargetElement($clicked.id)
        }

        debug('$clicked', $clicked)
    }

    /** @param {MouseEvent} e */
    openSubmenu(e) {
        if (!e) return;
        const x = e.pageX
        const y = e.pageY
        const $target = (document.elementFromPoint(x, y))
        if ($target === this.#menu) return;
        const $li = (/** @type {HTMLElement} */
            (e.target)).closest('li')

        Array.from(this.#menu.children).forEach($child => {
            const $dropdown = (/** @type {HTMLElement} */ ($child.querySelector('menu.dropdown')))
            if (!$dropdown) return;
            if ($li === $child) {
                $dropdown.style.visibility = 'visible'
                this.isOpen = true
            }
            else $dropdown.style.visibility = 'hidden'
        })
    }

    /** 
     * @param {string} element
     * @param {HTMLElement} [$]
     */
    setBlocktextElement(element, $) {
        if (!element) return;
        $ = $ ? $ : (this.#target || undefined)
        if (!$) return;

        if (element === 'blocktext-quote')
            this.replaceTargetElement('blockquote')
        if (element === 'blocktext-paragraph')
            this.replaceTargetElement('p')
        if (element === 'blocktext-header') {
            let $drawer = /** @type {HTMLElement} */
                (this.#blocktextMenu.querySelector('menu.drawer'))
            if (!$drawer) return;
            $drawer.style.visibility = ($drawer.style.visibility === 'visible')
                ? 'hidden' : 'visible'
        }
    }

    /** 
     * @param {string} element
     * @param {HTMLElement} [$target]
    */
    replaceTargetElement(element, $target) {
        if (!element) return;
        $target = $target ? $target : (this.#target || undefined)
        if (!$target) return;
        try {
            let $ = document.createElement(element)
            $.innerHTML = $target.innerHTML
            $target.replaceWith($)
            this.#editable['setTarget']($)
            this.#target = $
        }
        catch {
            console.warn(`Failed to create '${element}' element.`)
        }
    }

    /** 
     * @param {string} style
     * @param {HTMLElement} [$]
     */
    setStyle(style, $) {
        if (!style) return;
        $ = $ ? $ : (this.#target || undefined)
        if (!$) return;
        console.log(style, $)

        if (style.endsWith('italic')) {
            this.#editable['toggleInlineStyle']('italic')
        }
        if (style.endsWith('bold')) {
            this.#editable['toggleInlineStyle']('bold')
        }
    }

    /** 
     * @param {string} alignment
     * @param {HTMLElement} [$]
     */
    setAlignment(alignment, $) {
        if (!alignment) return;
        $ = $ ? $ : (this.#target || undefined)
        if (!$) return;
        if (alignment === 'align-left') {
            $.style.textAlign = (this.#direction === 'ltr') ? '' : 'left'
            $.style.textAlignLast = ''
            cleanElement($)
        }
        else if (alignment === 'align-center') {
            $.style.textAlign = 'center'
            $.style.textAlignLast = ''
            cleanElement($)
        }
        else if (alignment === 'align-right') {
            $.style.textAlign = (this.#direction === 'rtl') ? '' : 'right'
            $.style.textAlignLast = ''
            cleanElement($)
        }
        else if (alignment === 'align-justify') {
            $.style.textAlign = 'justify'
            $.style.textAlignLast = 'justify'
            cleanElement($)
        }
        else if (alignment === 'align-left-justify') {
            $.style.textAlign = 'justify'
            $.style.textAlignLast = (this.#direction === 'ltr') ? '' : 'left'
            cleanElement($)
        }
        else if (alignment === 'align-center-justify') {
            $.style.textAlign = 'justify'
            $.style.textAlignLast = 'center'
            cleanElement($)
        }
        else if (alignment === 'align-right-justify') {
            $.style.textAlign = 'justify'
            $.style.textAlignLast = (this.#direction === 'rtl') ? '' : 'right'
            cleanElement($)
        }
    }
}

customElements.define('editor-toolbar', EditorToolbar)

/** @param {HTMLElement} $ */
function isBlocktextElement($) {
    const blocktextElements = ['p', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']
    return Boolean(blocktextElements.includes($.tagName.toLowerCase()))
}

/** @param {HTMLElement} $ */
function mayBeAligned($) {
    const exceptions = ['li']
    return Boolean(exceptions.includes($.tagName.toLowerCase()) || isBlockElement($))
}

/** @param {Element|HTMLElement} $ */
function isBlockElement($) {

    const display = ('style' in $ && $.style.display)
        || window.getComputedStyle($, '')?.display
    debug('elementType:', display)

    return Boolean(
        ('style' in $ && $.style.display === 'block') ||
        window.getComputedStyle($, '')?.display === 'block'
    )
}

/** @param {Element} $ */
function cleanElement($) {
    if ($.hasAttribute('class') && !$.classList.length)
        $.removeAttribute('class')
    if ($.hasAttribute('style') && !$.getAttribute('style'))
        $.removeAttribute('style')
}

// Todo: fix bugs:
// * Drawer may hang open when changing submenus.