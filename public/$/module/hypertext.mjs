import * as UFO from '../lib/ufo@1.3.2/index.mjs';

export const version = '0.21';
export const run = HypertextElement
export const get = getDocument
export default {
    version, run, get, integrate
}

const hypertext = HypertextDocument()
function HypertextDocument() {
    /** @type {HTMLBodyElement} */
    let $body

    return {
        get body() { return $body },
        set body($) {
            if ($body instanceof HTMLBodyElement ||
                !($ instanceof HTMLBodyElement)) return;
            $body = $
        }
    }
}

/**
 * @typedef {Object} HypertextDocumentProperties
 * @property {string} etag
 * @typedef {Document & Partial<HypertextDocumentProperties>} HypertextDocument */

/** 
 * @typedef {Object} HypertextElementProps
 * @property {DocumentFragment} fragment
 * @property {String} href
 * @property {String} selector
 * @property {() => Promise<Element>} cloneSourceElement
 * @property {String[]} scopedStyles
 * @property {Function[]} appliedScripts
 * @property {Function} run
 * @property {Function} render
 * @property {Function} renderChildren
 * @property {boolean} rendered
 * @typedef {Element & Partial<HypertextElementProps>} HypertextElement */

/**
 * @param {HypertextElement} $hypertext
 * @param {Partial<{useAppliedScripts:boolean}>} [options] */
export async function HypertextElement($hypertext, options) {
    if (!isQualified()) return;
    const observer = new MutationObserver(respond)
    const document = $hypertext.ownerDocument;
    const config = {
        useAppliedScripts: Boolean(!(options?.useAppliedScripts === false))
    }
    
    let url = UFO.createURL()
    let /** @type {HypertextDocument|undefined} */
        context;
    let /** @type {HypertextDocument|undefined} */
        source;
    let /** @type {Element|undefined} */
        $source;
    let /** @type {Function[]} */
        appliedScripts = [];
    let /** @type {Function} */
        render = () => {};
    let rendered = false;

    defineProperties($hypertext, {
        cloneSourceElement: {
            value: cloneSourceElement
        },
        run: {
            value: run
        },
        render: {
            get: () => render
        },
        renderChildren: {
            value: renderChildren
        },
        rendered: {
            get: () => rendered
        },
        selector: {
            get: () => deriveSelector($hypertext)
        },
        href: {
            get: () => url.toLocaleString()
        },
        appliedScripts: {
            get: () => appliedScripts
        }
    });

    await run();
    async function run() {
        console.log('Hypertext.run:', $hypertext)
        setUrl()

        if ($hypertext.getAttribute('etag')) {
            console.log("HypertextElement has 'etag' attribute; deferring re-render.")

            //? walk context.html to confirm document?

            $hypertext.addEventListener('rendered', () => {
                //todo: request cache to test freshness of etag.
            }, { once: true })
        }
        else {
            $source = await cloneSourceElement()
                //? || $hypertext.cloneNode(true))
            if ($source) {
                document.adoptNode($source);
                if (document?.doctype?.name === 'hypertext')
                    integrate($hypertext, $source);
                else {
                    const $context = await getContext()
                    //? console.log('$source:', $source)
                    //? console.log('$context:', $context)
                    integrate($context, $source)
                    $source.setAttribute('href', url.pathname)
                }
                if (source?.etag)
                    $source.setAttribute('etag', source.etag)
            }
        }

        await HypertextChildren();
        selectBody();
        useScopedStyles();
        if (config.useAppliedScripts)
            useAppliedScripts();

        render = () => {
            render = () => {};
            $hypertext.dispatchEvent(new Event('render'))
            if ($source) {
                inheritAttributes($source, $hypertext)
                //? console.log('replacing:', $hypertext)
                //? console.log('render:', $source)
                $hypertext.replaceChildren(...Array.from($source.childNodes))
                $source.remove()
                $source = undefined
            }
            $hypertext.dispatchEvent(new Event('rendered'))
            rendered = true
        }

        $hypertext.addEventListener('rendered', 
            observe, { once: true })
    }


    function observe() {
        observer.disconnect()
        observer.observe($hypertext, {
            attributeFilter: ['href'],
            attributeOldValue: true,
            subtree: false
        })
    }

    /** @param {MutationRecord[]} mutations */
    function respond(mutations) {
        mutations.forEach(async (mutation) => {
            //? console.log('mutation:', mutation)            
            if (mutation.type === 'attributes' &&
                mutation.attributeName === 'href' &&
                (mutation.target instanceof Element) &&
                (mutation.target.getAttribute('href') !== mutation.oldValue))
            {
                console.log('mutated:', mutation.target)
                observer.disconnect()
                $hypertext.removeAttribute('etag')
                await run()
                await startViewTransition($hypertext.render)
            }
            /* if (mutation.type === 'characterData' &&
                Object.is($style, mutation.target.parentElement)
            )   updateScope() */
        })
    }


    function renderChildren() {
        /** @type {HypertextElement[]} */
        const $children = Array.from($hypertext.querySelectorAll('[href]'))
            .filter($ => !$.hasOwnProperty('href'))
        $children.forEach($child => $child.render?.())
    }

    function isQualified($=$hypertext) {
        if (!($ instanceof Element)) return false;
        if (Object.getPrototypeOf($).hasOwnProperty('href')) return false
        return true
    }

    async function getContext() {
        const href = UFO.joinURL(location.origin, location.pathname, 'context.html')
        context = await getDocument(href)
        if (!context) return;
        const selector = deriveSelector($hypertext, {
            relativeTo: context
        })
        return querySelector(selector, context)
    }

    /** @param {Element} $ */
    function setUrl($=$hypertext) {
        let href = $.getAttribute('href') || ''
        if (UFO.isRelative(href)) {
            href = UFO.joinURL(location.pathname, href)
            $.setAttribute('href', href)
        }
        url = UFO.createURL(href);
        if (!url.host) url.host = location.host
    }
    
    async function cloneSourceElement() {
        source = await getDocument(url.href);
        const selector = (source) ? (() => {
            if ($hypertext.hasAttribute('select')) {
                const specified = $hypertext.getAttribute('select')
                if (specified && isUniqueSelector(specified, source))
                    return specified;
            }
            return [
                $hypertext.selector,
                deriveSelector($hypertext, { relativeTo: source }),
                //? templateSelector, is
                'template', 'body'
            ].find(selector => isUniqueSelector(selector, source))
        })() : null
        //? possibly do not clone if always fetching anew . . let cache cache the document.
        //  const $source = querySelector(selector, source)

        return /** @type {Element|undefined} */(
            querySelector(selector, source) || undefined)
    }

    function selectBody() {
        if (hypertext.body) return;
        hypertext.body = /** @type {HTMLBodyElement} */(
            ($source instanceof HTMLBodyElement)
                ? $source
                : $source?.querySelector('body')
                    || document.body)
    }

    function useAppliedScripts() {
        [...Array.from($hypertext.querySelectorAll('script[applied]')),
         ...Array.from($source?.querySelectorAll('script[applied]') || [])]
            .forEach($script => {
                if (!($script instanceof HTMLScriptElement)) return;
                try {
                    const $parent = $script.parentElement
                    const $bind = /** @type {Element} */
                        ((Object.is($parent, $source))
                            ? $hypertext : $parent)
                    if (!$bind) throw new Error("Unbound 'applied' script has no parent element.")
                    appliedScripts.push(
                        Function($script.innerText)
                            .bind($bind))
                }
                catch (error) { console.warn(error) }
                $script.remove()
            })

        $hypertext.addEventListener('rendered', () => {
            //? console.log("Running appliedScripts:", appliedScripts)
            appliedScripts.forEach(script => {
                try {
                    script()
                }
                catch (error) {
                    console.warn(script, error)
                }
            })
        }, {
            once: true
        })
    }

    function useScopedStyles() {
        hoisted: {
            const $styles = /** @type {HTMLStyleElement[]} */([])
            Array.from($hypertext.querySelectorAll('&> style[scoped')).forEach($style => {
                if (!($style instanceof HTMLStyleElement)) return;
                useScopedStyle($style, $hypertext)
                $style.remove(); $styles.push($style)
            })

            if (!$styles.length) break hoisted;
            $hypertext.addEventListener('render', () => {
                while ($styles.length) {
                    const $style = $styles.shift()
                    if ($style) hypertext.body.append($style)
                }
            }, { once: true })
        }

        nested: {
            if (!$source || Object.is($hypertext, $source)) break nested;
            Array.from($source.querySelectorAll('style[scoped]'))
                .forEach($style => useScopedStyle($style, $style.parentElement))
        }

        /**
         * @param {HTMLStyleElement|any} $style
         * @param {Element|any} $target */
        function useScopedStyle($style, $target) {
            if (!($target instanceof Element)) return;
            if (!($style instanceof HTMLStyleElement)) return;
            const style = $style.textContent; if (!style) return;
            if (!style.match(/\:scope/)) return;
            const hash = hashStyle(style)
            addAttributeValue($target, 'scope', hash)
            $style.textContent = style
                .split(':scope').join(`[scope~="${hash}"]`)
            //? console.log(`[scope~="${hash}"] scoped to:`, $target)
        }
    }

    /**
     * @param {string|any} selector 
     * @param {Element|Document|DocumentFragment} context */
    function isUniqueSelector(selector, context=document) {
        if (!(selector && typeof selector === 'string')) return false;
        if (!context?.querySelectorAll) return false;
        if (context.querySelectorAll(selector).length === 1) return true;
        return false;
    }

    function HypertextChildren() {
        const $ = $source || $hypertext
        /** @type {HypertextElement[]} */
        const children = Array.from(
            $.querySelectorAll('[href]'))
                .filter(isQualified)
        return Promise.allSettled(children.map(async ($child) => {
            await HypertextElement($child, config).then(() => {
                if ($child.render instanceof Function)
                    $child.render()
            })
        }))
    }
}

/**
 * @description: Verify href; fetch html; parse Document.
 * @param {string|any} href */
async function getDocument(href) {
    if (!(href && typeof href === 'string'))
        return;
    /** @type {string|undefined} */ let html;
    /** @type {string|undefined} */ let etag; 

    try {
        const response = await fetch(href, { mode: 'no-cors' })
        if (!response.ok) throw new Error();
        html = await response.text()
        etag = response.headers.get('etag')?.match(/(?<=").+(?=")/)?.shift()
    }
    catch {
        console.warn("Failed to fetch:", `'${href}'`)
        return;
    }
    try {
        /** @type {HypertextDocument} */
        const source = parser.parseFromString(html, 'text/html')
        Object.defineProperty(source, 'etag', {
            value: etag, writable: false
        })
        //? console.log('source.etag:', source.etag)
        return source
    }
    catch { console.warn(`Failed to parse:`, html) }
    return;
}
const parser = new DOMParser()

/**
 * @param {Element|DocumentFragment|Document|any} source
 * @param {string|any} selector */
function querySelector(selector, source=document) {
    return /** @type {Element|null} */((
        source.querySelector instanceof Function &&
        selector && typeof selector === 'string'
    ) ? source.querySelector(selector) : null)
}

/**
 * @param {string|any} selector 
 * @param {Element|Document|DocumentFragment} context */
function isUniqueSelector(selector, context=document) {
    if (!(selector && typeof selector === 'string')) return false;
    if (!context?.querySelectorAll) return false;
    if (context.querySelectorAll(selector).length === 1) return true;
    return false;
}

/** 
 * @param {Element} $target
 * @param {Partial<{ relativeTo: Element|Document|DocumentFragment, complete: boolean }>} [options] */
function deriveSelector($target, options) {
    if (!($target instanceof Element)) return '';
    if ($target instanceof HTMLHtmlElement) return ':root'

    const context = (typeof options?.relativeTo?.['querySelectorAll'] === 'function')
        ? options.relativeTo : $target.ownerDocument;

    let selector = ''
    let $ = (/** @type {Element|null} */($target))

    const getTag = () => $?.localName || ''
    const getID = () => $?.getAttribute('id') ? `#${$.getAttribute('id')}` : '' || ''
    const getClass = () => $?.getAttribute('class') ? '.' + Array.from($.classList).join('.') : '' || ''
    const getNth = () => (Array.from($?.parentElement?.querySelectorAll(getTag()) || []).length > 1)
        ? `:nth-child(${1 + Array.from($?.parentElement?.children || [])
            .findIndex($el => Object.is($el, $))})` : ''

    function complete() {
        while ($ && !($ instanceof HTMLHtmlElement) && !(Object.is($, context))) {
            selector = [getTag(), getID(), getClass(), getNth(),
            (selector ? `> ${selector}` : '')
            ].join('')
            $ = $.parentElement
        }
        return selector
    }
    function succinct() {
        /** @param {string} s */
        const getSelector = (s) =>
            s + (selector ? `> ${selector}` : '')
        const isUnique = (s = selector) => Boolean(s &&
            context.querySelectorAll(s).length === 1)

        while ($ && !($ instanceof HTMLHtmlElement) && !(Object.is($, context))) {
            const id = getID()
            if (id && isUnique(getSelector(id)))
                return getSelector(id)

            const tag = getTag()
            if (isUnique(getSelector(tag)))
                return getSelector(tag)

            const classes = getClass()
            if (tag && classes && isUnique(getSelector(tag + classes)))
                return getSelector(tag + classes)

            const nth = getNth()
            if (nth && isUnique(getSelector(tag + nth)))
                return getSelector(tag + nth);

            selector = [tag, id, classes, nth, selector ? `> ${selector}` : ''].join('')
            if (isUnique()) return selector
            $ = $.parentElement
        }
        return complete()
    }
    return (options?.complete === true)
        ? complete() : succinct()
}

/** 
 * @typedef {Object} inhheritAttributesOptions
 * @property {string[]} [except]
 * @param {Element|any} $source
 * @param {Element|any} $target
 * @param {inhheritAttributesOptions} [options] */
function inheritAttributes($source, $target, options) {
    if (!($source instanceof Element) ||
        !($target instanceof Element)) return;
    const attributes = $source.getAttributeNames()
        .filter(attribute => !(options?.except?.includes(attribute)))
    attributes.forEach(attribute => {
        const value = $source.getAttribute(attribute) || ''
        if ($target.getAttribute(attribute) !== value)
            $target.setAttribute(attribute, value)
    })
}

/**
 * @param {Element} $
 * @param {String} attribute
 * @param {any} value */
function addAttributeValue($, attribute, value) {
    if (typeof value === 'undefined') return;
    const values = new Set($.getAttribute(attribute)
        ?.trim().replaceAll(/(\s+)/g, ' ').split(' '))
    values.add(String(value))
    $.setAttribute(attribute, Array.from(values).join(' '))
}

/**
 * @param {Element} $
 * @param {String} attribute
 * @param {any} value */
function removeAttributeValue($, attribute, value) {
    if (typeof value === 'undefined') return;
    const values = new Set($.getAttribute(attribute)
        ?.trim().replaceAll(/(\s+)/g, ' ').split(' '))
    values.delete(String(value))
    $.setAttribute(attribute, Array.from(values).join(' '))
}

/** @param {string|any} string */
function hashStyle(string) {
    if (!(typeof string === 'string')) return;
    const hash = simpleHash(string)
    //* HTML/CSS ID cannot begin with a number:
    const letters = 'JABCDEFGHI'.split('')
    return hash.replace(/^[\d]/, letters[Number(hash.slice(0, 1))])
}

/** @param {string} string */
function simpleHash(string) {
    let hash = 0;
    for (let i = 0; i < string.length; i++) {
        const char = string.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash &= hash; // Convert to 32bit integer
    }
    return new Uint32Array([hash])[0].toString(36)
}//? https://gist.github.com/jlevy/c246006675becc446360a798e2b2d781

/**
 * @param {Object} obj
 * @param {PropertyDescriptorMap} properties*/
function defineProperties(obj, properties) {
    Object.entries(properties).forEach(([property, descriptor]) => {
        if (!obj.hasOwnProperty(property))
            Object.defineProperty(obj, property, descriptor)
    })
}

/**
 * @param {Element|any} $source 
 * @param {Element} $target */
export function integrate($source, $target) {
    if (!($source instanceof Element)) return;
    if ($source.isEqualNode($target)) return;
    inheritAttributes($source, $target)
    if (!$source.hasChildNodes) return;
    
    if (!$target.children.length &&
        !$source.children.length &&
            $source.textContent &&
        !($source instanceof HTMLScriptElement) &&
        !($source instanceof HTMLScriptElement)
    )   $target.textContent = $source.textContent

    Array.from($source.children).forEach($s => {
        const selector = deriveSelector($s, {
            relativeTo: $target
        })
        const $t = querySelector(selector, $target)
        if ($t) integrate($s, $t)
    })
}


/** 
 * @param {Function|any} updateDOM 
 * @returns {Promise<any>} */
function startViewTransition(updateDOM) {
    if (!(updateDOM instanceof Function)) {
        console.warn("Provide a Function that updates the DOM to 'startViewTransition'.")
        return Promise.resolve();
    }
    return ('startViewTransition' in document) // @ts-ignore:
        ? document.startViewTransition(updateDOM).finished
        : Promise.resolve(updateDOM())
}