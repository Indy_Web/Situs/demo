import config from '../config.mjs';
import { rpc } from './ipfs.mjs';
import * as Hypertext from './hypertext.mjs';

export const site = await Site(config.site.domain);
export default { site };

Object.defineProperty(window, 'IPFS', {
    value: Object.freeze({ rpc, site }),
    writable: false
})

/** 
 * @param {string} domain */
async function Site(domain) {
    if (!(rpc && typeof domain === 'string')) return null;

    const address = await rpc.name.resolveAll(domain)
    const dir = `/www/${domain}` //? || `/ipns/${key}`
    const key = address.ipns
    let cid = address.ipfs

    const isAuthor = (key) ? await rpc.key.has(key) : false
    if (isAuthor && cid) await rpc.files.write(cid, dir)

    return Object.freeze({
        get domain() {
            return domain
        },
        get dir() {
            return (isAuthor) ? dir : undefined
        },
        get key() {
            return key
        },
        get cid() {
            return cid
        },
        get isAuthor() {
            return isAuthor
        },
        publishFile: (isAuthor)
            ? publishFile
            : undefined,
        publishContext: (isAuthor)
            ? publishContext
            : undefined,
        queryDirectory: (isAuthor)
            ? queryDirectory
            : undefined,
    })

    /**
     * @param {string} pathname
     * @param {string|Blob} content 
     * @param {Partial<{name:string,type:string}>} [options] */
        async function publishFile(pathname, content, options={}) {
            console.log('publishFile:', pathname)

            if (!(key && rpc)) return;
            const name = pathname.match(/[^\/]+$/)?.shift()
            if (!name) return;
            if (!options.name) options.name = name
            const file_cid = (await rpc.addFile(content, options)).Hash
            const path = pathname.startsWith(dir) ? pathname : dir+pathname
            await rpc.files.write(file_cid, path)
            const site_cid = (await rpc.files.stat(dir))?.Hash
            await rpc.name.publish(site_cid, key)
            console.log(`published '${pathname}':`)
            console.log(`ipfs://${site_cid}${pathname}`)
            console.log(`ipns://${domain}${pathname}`)
            //? return site_cid

        // Todo: Saving document should pin content to IPNS key.
    }

    /** @param {string} route */
    async function publishContext(route) {
        console.log('publishContext:', route)
        if (!route.endsWith('/'))
            route = route+'/'
        const path = {
            context: route+'context.html',
            index: route+'index.html'
        }
        console.dir(path)
        const context = await Hypertext.get(location.origin+path.context)
        if (!context) return;
        /** @type {Hypertext.HypertextElement} */
        const $context = context.documentElement
        console.dir({ $context: $context.outerHTML})
        await Hypertext.run($context, {
            useAppliedScripts: false
        })
        if ($context.render)
            $context.render()
        else return;
        console.log('rendered')
        await publishFile(path.index, $context.outerHTML, {
            name: 'index.html', type: 'text/html'
        })
    }

    /** @param {string} path */
    async function queryDirectory(path) {
        const files = await rpc?.files.ls(dir+path) || []
        return files
    }
}