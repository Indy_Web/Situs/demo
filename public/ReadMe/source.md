---
is: article
title: ReadMe
---

# Situs
Situs is an in-browser web authoring framework for independent authors.

The codebase is built with 100% web-native standards that avoid the need for a build step:
  * native `.html`, _hypertext_, not JSON APIs as the _medium_ of communication
  * native `.css` (now has nesting selectors) instead of a preprocessor
  * native `.js` with JSdoc annotations and linting via  `// @ts-check` for type-_sanity_

Because the entire codebase is web-native, source files are immediately ready for release,
are human _readable_, thus decipherable and _hackable_. Exercise your right to repair and mod.

## "Site Generator"
Because Situs makes web authoring simple, some would call this a "_static_ site generator",
but we disagree with this and prefer to call it rather just a **"site generator"**, because:
  1. Situs provides a mutable container, a _site_ for publishing _dynamic_ web content.
    A. Situs does not watch a source, having to re-_build_ a static repository for release.
    B. Situs provides a web-native environment for reading/writing to a dynamic source:
      * an IPNS name identifying mutable IPFS content addressing the site's source.

## Development Setup:
Because Situs is web-native simply _serve_ the `public` directory, however you wish.

The recommendation is to:
 1. Install IPFS Desktop; Enable Brave IPFS node via `brave://ipfs`
 2. Ensure that Node installed and `http-server` is available globally on the commandline.
 3. Download or pull the repo.
 4. From the root of the repo, evoke the scripts defined in package.json:
   - `npm run dev`
   - `npm run release`

## Minimal Setup:

<!--

  * Hypertext fetches the source template,
  * applies the inner template to the source,
  * and writes the result to the document.

  * Hypertext is an instruction notation for modifying the document.
  * The `hypertext.mjs` module must be included:
    -`<script type="module" src="/$/module/hypertext.mjs"></script>`
  
  * Hypertext attributes:
    * 'src':
      * declares that the element is hypertext; triggers new instance
      ->fetches resource, which is merged into document at target element.
    * 'target' (optional) explicitly selects target element within current document
      - without 'target', selector is naively generated according to innerHTML template

  * # The Hypertext Lifecycle:
        1. an `.html` document with the `hypertext.mjs` module included is loaded into the browser as usual
        2. the `hypertext.mjs` module runs, which selects the <html> and declares as hypertext, which initializes root instance
        3. if the hypertext element has 'src' attribute:
            a. source is fetched as html string
            b. html string parsed and rendered as document fragment
            c. children of hypertext element are merged into fragment
            d. selector is naively generated unless explicitly declared on the hypertext element
            e. the fragment is merged into the current document at selected target
            f. elements with `src` attribute (except: script, img) are selected and initalized as hypertext instances
        4. hypertext element acts "like a component" by employing scoped CSS and applied JS:
            a. 'script[scoped]' is selected;
            b. unique hash is generated from 'style[scoped]'.innerText

        5. the hypertext instance observes mutations to itself for specific changes that trigger actions:
            a. mutation of `src` attribute will trigger the hypertext to re-render and replace its content.
            b. addition of a `src` attribute to an element:
                - declares the element to be hypertext, initializing a new Hypertext instance
            c. removal of a `src` attribute from an element:
                - destroys the Hypertext instance leaving the document in its state:
                    - removes record of instance from static Hypertext `references`
         . . . 


    - # Merge process:
        1, DOM.merge($source, $target)
            A. Ensure that elements are same types

    - # selector determination: $target selector is either:
        A. automatically generated as expression of template,
            
        These are keys for automatically generating selectors:
        - unique elements (occur only once):
            * html
            * html> head
            * head> base
            * head> title
            * head> meta[name="?"]
            * meta[?]:not([name]):not([content])
                - unique generic meta (does not have 'name' or 'content' defined)
            * script[src="?"]
                - only allow script to initialize once on the document
            * link[href="?"]
                - specific stylesheets set only once on the document
            * html> body
            * body main




        B. explicitly set as 'target' attribute.


    * # scoped CSS process:
        Each <style scoped> is applied to it's immediate parent.

        0. Immediately disable 'style[scoped]' elements on page load
            - :scope selector is broken and refers to root element
        1. All 'style[scoped]' elements that are immediate child of $hypertext are selected.
            - innerText of each are joined together by `\n` into single style tag.
                - additional style[scoped] elements removed.
        2. generate hash from innerText and slice to 5 characters 'scope' id 
        3. innerText.replace(':scope', '[scope="#####"]')
        4. $hypertext.setAttribute('scope', '#####')
        5. Rest 'disable': set to false.
        
-->