'encursive' (adj.) —runs/writes into/within itself; makes "encursions"
    from Greek [_en-_](https://www.etymonline.com/word/*en) "in", _eis_ "into", _endon_ "within"
    and ["-cursive"](https://www.etymonline.com/word/cursive), from PIE root <a href="https://www.etymonline.com/word/*kers-">*kers-</a> "to run" from Latin _cursivus_ "running", as in the phrase "written with a running hand".


The definition of 'encursive' distinguishes itself from:
 * 'recursive', which means to "run back". The meaning of 'encursive' is more than what 'recursive' implies in the context of programming. Hypertext elements are blank tablets, surfaces for inscribing, _writing into_; as the functions of the program recur, the Hypertext engine makes _encursions_, sudden movements of writing into itself.

 * 'incursive', as in "making incursions", and 'incursion', as in an "hostile attack" or "invasion". These connotations of 'incursive', which imply a movement _outside_ of or _away from_ one's own place into a foreign territory that is by definition hostile, are irrelavant to the meaning of 'encursive', or the Situs hypertext engine, which runs _within_ the scope of a site, having established its own address and jurisdiction; see: _situs_ (n.) —"proper or original position and location of something".

In the context of the Situs hypertext engine, 'encursive' describes its way of writing into itself.