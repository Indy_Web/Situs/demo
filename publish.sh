#! /bin/bash

dir="./public"
config="./config.json"
key="$(jq -r '.site.key' $config)"
domain="$(jq -r '.site.domain' $config)"
multiaddr="$(jq -r '.ipfs.rpc.multiaddr' $config)"
api="--api $multiaddr"
mfs="/www/$domain"

echo
echo "Publishing \"$dir\""
echo "via $multiaddr"

#IPFS
start="$(date +%s)"
ipfs="$(ipfs $api add -rQ $dir)"
echo "to ipfs://$ipfs"

if command -v xclip &> /dev/null
then echo "ipfs://$ipfs" | xclip -selection clipboard
fi
if [[ "$1" == "ipfs" ]]; then exit 0; fi

#MFS:
ipfs $api files stat $mfs >/dev/null 2>&1
test $? -eq 1 || ipfs $api files rm -r $mfs
ipfs $api files cp /ipfs/$ipfs $mfs
echo "to mfs://$mfs"
if [[ "$1" == "mfs" ]]; then exit 0; fi

#IPNS:
ipns="$(ipfs $api name publish -Q --key $key $ipfs)"
ellapsed="$(($(date +%s)-$start))"
echo "to ipns://$ipns"
echo "as ipns://$domain"
echo "in $ellapsed seconds."
if [[ "$1" == "ipns" ]]; then exit 0; fi

echo
echo "Publishing to http://$domain"
surge $dir $domain