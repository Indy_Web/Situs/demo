#! /bin/bash

config="./config.json"
domain="$(jq -r '.site.domain' $config)"
multiaddr="$(jq -r '.ipfs.rpc.multiaddr' $config)"
api="--api $multiaddr"
repo="/www/$domain"
target="./public"

echo "Resolving ipns://$domain:"
content=$(ipfs $api name resolve $domain)
echo "to $content"

ipfs $api get -o $target $content
ipfs $api files rm -r $repo
ipfs $api files cp -p=true $content $repo