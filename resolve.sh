#! /bin/bash

config="./config.json"
domain="$(jq -r '.site.domain' $config)"
multiaddr="$(jq -r '.ipfs.rpc.multiaddr' $config)"
api="--api $multiaddr"

echo "Resolving ipns://$domain:"
ipns=$(ipfs $api name resolve -r=false $domain)
ipfs=$(ipfs $api name resolve $domain)
echo $ipns
echo $ipfs