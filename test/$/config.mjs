export default {
    site: {
        domain: 'situs.run',
        //? domain is optional, whereas IPNS key is primary. Should accomodate domain-less IPNS hosting first.
    },
    ipfs: {
        //?  The RPC server is device-dependent. Hardcode default; provide means of configuring via LocalStorage.
        rpc: 'http://127.0.0.1:45005/api/v0'
    }
}