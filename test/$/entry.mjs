import './module/element/dynamic-icon.js';
import Situs from './module/situs.mjs';
import Editable from './module/directive/editable.mjs';

await Situs.loadDocument();
Situs.useNavigation();
Editable.apply();
