import {LitElement, html } from '/$/lib/lit@3.0.2/lit-all-min.js';


console.log('file-tree.mjs')

class LitFile extends LitElement {
    static properties = {
        name: { type: String },
        type: { type: String },
        cid: { state: true },
    };

    render() {
        return html`<li class="file">${icon()}<span class="name">${this.name}</span></li>`;

        function icon() {
            const iconFileName = () => {
                if (this.type.endsWith('html'))
                    return 'file-html-ph.svg'
                if (this.type.endsWith('javascript'))
                    return 'file-js-ph.svg'
                if (this.type.startsWith('image'))
                    return 'file-image-ph.svg'
                return 'file-ph.svg'
            }
            return html`<dynamic-icon src="/$/icon/${iconFileName}"></dynamic-icon>`
        }
    }
}

class LitDirectory extends LitElement {
    static properties = {
        name: { type: String }
    }

    render() {
        return html`<span>${this.name}</span><ul><slot></slot></ul>`
    }
}


customElements.define('l-file', LitFile)
customElements.define('l-directory', LitDirectory)
Object.defineProperty(window, 'constructFileTree', {
    value: constructFileTree
})

/**
 * @description:
 *  * Uses IPFS MFS path to list and stat dirents
 *  * Creates nested list elements.
 */


const excluded = [
    '/$/lib'
]


async function constructFileTree(path='/www/situs.run') {
    if (path.endsWith('/')) path = path.replace(/\/$/, '')
    if (excluded.find(suffix => path.endsWith(suffix))) return null;
    const { name, type, cid } = await stat(path)
    if (!(name && type && cid)) return null;

    if (type !== 'directory') {
        const $file = document.createElement('l-file')
        $file.setAttribute('name', name)
        $file.setAttribute('type', type)
        $file.setAttribute('cid', cid)
        /* Object.defineProperty($file, 'cid', {
            value: cid
        }) */
        return $file;
    }
    else {
        const $directory = document.createElement('l-directory')
        $directory.setAttribute('name', name, 'cid', cid)
        const dirents = await IPFS.rpc.files.ls(path)
        const $dirents = new Map()
        dirents.forEach(name => $dirents.set(name))
        await Promise.allSettled(dirents.map(async (name) => {
            const $dirent = await constructFileTree(`${path}/${name}`)
            if ($dirent) $dirents.set(name, $dirent)
        }))
        Array.from($dirents.entries()).forEach(([name, $dirent]) => {
            if ($dirent) $directory.append($dirent)
        })
        return $directory
    }
}


const gateway = 'http://127.0.0.1:48084'
//todo: configurable/deterministic gateway.

/** @param {string} path */
async function stat(path) {
    const dirent = await IPFS.rpc.files.stat(path)
    const name = /[^\/]+(?=($|\/$))/.exec(path)?.shift() || null
    if (!(name && ['file', 'directory'].includes(dirent.Type))) return {};
    const cid = dirent.Hash
    const type = await getType()
    return { name, cid, type }

    async function getType() {
        if (dirent.Type === 'directory') return 'directory'
        const response = await fetch(`${gateway}/ipfs/${cid}`, { method: 'HEAD' })
        return response.headers.get('content-type')?.split(';').at(0) || null
    }
}
