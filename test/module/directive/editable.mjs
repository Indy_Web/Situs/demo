import Situs from '../site.mjs'
import * as UFO from '../../lib/ufo@1.3.2/index.mjs';

/** 
 * @description Directive applied to Element with 'editable' attribute.
 *              '$source' attribute inherited from HypertextElement
 * @typedef {Element & Partial<{editor: Editor, cloneSourceElement: ()=>Promise<Element>, href: string}>} EditableElement
 * 
 * @typedef {Object} Editor
 * @property {boolean} enabled
 * @property {Function} enable
 * @property {Function} disable
 * @property {boolean} edited
 * @property {Function} saveDocument
 * 
 * @typedef {Object} EditableBlock
 * @property {Function} enable
 * @property {Function} disable
 * @property {string} savedOuterHTML
 * @property {boolean} edited
 * */


const debug = Debug()

const Style = /*css*/
`:host {
    position: relative;
}
dynamic-icon {
    opacity: 0;
    width: 1.25em;
    position: absolute; top: 1em;
    &#editor-enable, &#editor-disable, &#editor-save { right: 1em; }
    &:hover   { 
        cursor: pointer;
    }
    transition: opacity 0.5s ease;
    &[visible]     { opacity: 1; }
    &:not([visible]) {
        pointer-events: none;
    }
}`

const Template = /*html*/ 
`<style>${Style}</style>
<dynamic-icon id="editor-enable" src="/$/icon/pencil.svg" fill="var(--color-bg-body)" title="Edit document."></dynamic-icon>
<dynamic-icon id="editor-disable" src="/$/icon/tab-close.svg" fill="var(--color-bg-body)" title="View document."></dynamic-icon>
<dynamic-icon id="editor-save" src="/$/icon/save.svg" fill="var(--color-bg-body)" title="Save document."></dynamic-icon>
<dynamic-icon id="editor-revert" src="/$/icon/trash-x-filled.svg" fill="var(--color-bg-body)" title="Revert document."></dynamic-icon>
<slot></slot>
<editor-toolbar/>`

export default { apply, EditableElement }

export function apply() {
    Array.from(document.querySelectorAll('[editable]'))
        .forEach($editable => EditableElement($editable))
}

/** @param {EditableElement} $editable */
export function EditableElement($editable) {
    if (!($editable instanceof Element)) return;
    console.log('EditableElement:', $editable)
    const document = $editable.ownerDocument;
    const $blocks = EditableBlocks()
    let $active = /** @type {Element|null} */(null)

    let enabled = false;
    Object.defineProperty($editable, 'editor', {
        value: Object.freeze({
            get enabled() {
                return enabled;
            },
            get edited() {
                return Boolean(Array.from($blocks.entries())
                    .find(([$, block]) => ($.isConnected && block.edited)))
            },
            enable,
            disable,
            saveDocument
        }),
        writable: false
    })

    if (!$editable.shadowRoot)
        $editable.attachShadow({ mode: 'open' });
    $editable.shadowRoot?.append(renderTemplate());

    const $enable = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-enable'));
    const $disable = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-disable'));
    const $save = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-save'))

    $save.onclick = save
    $enable.onclick = () => {
        enable()
        $enable.removeAttribute('visible')
        $disable.setAttribute('visible', '')
    }
    $disable.onclick = () => {
        disable();
        $disable.removeAttribute('visible');
        $enable.setAttribute('visible', '');
    }

    $editable.addEventListener('mouseenter', (e) => {
        if (!enabled) $enable.setAttribute('visible', '');
    })
    $editable.addEventListener('mouseleave', (e) => {
        if ($enable.hasAttribute('visible'))
            $enable.removeAttribute('visible');
    })

    function enable() {
        enabled = true;
        $editable.setAttribute('spellcheck', 'true');
        $editable.addEventListener('click', handleClick);

        $editable.addEventListener('edited', handleEdited);
    }

    /** @param {Event} e */
    function handleEdited(e) {
        console.log('editedEvent:', e)

        if ($editable.editor?.edited && !$save.hasAttribute('visible')) {
            $disable.removeAttribute('visible')
            requestAnimationFrame(() =>
                $save.setAttribute('visible', ''))
        }
    }

    function disable() {
        enabled = false;
        disableBlocks();
        $editable.removeAttribute('spellcheck');
        $editable.removeEventListener('click', handleClick);
    }

    function save() {
        disable();
        saveDocument();
    }

    function EditableBlocks() {
    /** @type {Map<Element, EditableBlock>} */
        const $blocks = new Map()
        Array.from($editable.querySelectorAll('*'))
            .filter($ => (getComputedStyle($)?.display === 'block'))
            .forEach($block => {
                $blocks.set($block, EditableBlock($block))
            })
        return $blocks
    }

    /** @param {Element} $block */
    function EditableBlock($block) {
        const savedOuterHTML = $block.outerHTML
        const block = Object.freeze({
            get savedOuterHTML() {
                return savedOuterHTML
            },
            get edited() {
                return Boolean($block.outerHTML !== savedOuterHTML)
            },

            enable,
            disable
        })

        function enable() {
            disableBlocks()
            $active = $block;
            $block.setAttribute('contenteditable', 'true');
            $block.addEventListener('input', handleInput);
        }
        function disable() {
            if ($block.isSameNode($active)) $active = null;
            if ($block instanceof HTMLElement) $block?.blur();
            $block.removeEventListener('input', handleInput);
            $block.removeAttribute('contenteditable');
            cleanElement($block);
        }

        /** @param {Event} event */
        function handleInput(event) {
            if (!(event instanceof InputEvent)) return;
            debug('inputEvent', event)
            if (block.edited)
                $editable.dispatchEvent(new CustomEvent('edited'))
        }

        return /** @type {EditableBlock} */(block)
    }

    function disableBlocks() {
        $editable.querySelectorAll('[contenteditable]')
            .forEach($ => $blocks.get($)?.disable());
    }

    /** @param {Element} $ */
    function parentBlockElement($) {
        const isBlock = () => $ ? getComputedStyle($)?.display === 'block' : false
        while (!isBlock()) {
            if ($?.isSameNode($editable)) return;
            if (!$.parentElement) return;
            else $ = $.parentElement
        }
        if (isBlock()) return $;
    }


    /** @param {Event} event */
    function handleClick(event) {
        if (!(event instanceof MouseEvent)) return;
        const $clicked = /** @type {HTMLElement} */
            (document.elementFromPoint(event.clientX, event.clientY))
        if (!$active?.contains($clicked)) {
            const $block = parentBlockElement($clicked)
            if ($block) $blocks.get($block)?.enable()
            console.log('clicked:', $clicked, $block)
        }

        /*
        if (e.ctrlKey)
            $toolbar['openMenu']($clicked)
        if (!e.ctrlKey && !$toolbar.contains($clicked))
            $toolbar['closeMenu']() */
    }


    async function saveDocument() {
        console.log('saveDocument()')
        // Todo: Require sanitization of innerHTML:
        //? const sanitized = DOMPurify.sanitize(html)
        //? const b = new Blob([sanitized])
        
        if (!Situs.site?.publishFile ||
            !Situs.site?.publishContext) return;
        const html = await Html();
        console.log('html:', html)
        if (!html) return;
        const path = UFO.createURL($editable.href).pathname
        const name = path.match(/[^\/]+$/)?.shift()
        console.dir({ path, name, html })
        if (!name) return;
        console.log('saving document:', path)
        await Situs.site.publishFile(path, html, {
            name, type: 'text/html'
        })
        console.log('path.replace(name, ""):', path.replace(name, ''))
        await Situs.site.publishContext(path.replace(name, ''))
        async function Html() {
            if (!($editable.cloneSourceElement instanceof Function)) return;
            const $source = await $editable.cloneSourceElement();

            //todo: fallback if $source not available.
            if (!($source instanceof Element)) return;
            $source.replaceChildren(
                cloneContents($editable))
            return $source.outerHTML
        }
    }
}


/** @param {string} html */
function renderTemplate(html=Template) {
    return document.createRange()
        .createContextualFragment(html)
}

/** @param {Element} $ */
function cloneContents($) {
    const range = $.ownerDocument.createRange()
    range.selectNodeContents($.cloneNode(true))
    return range.extractContents()
}

function Debug(debug=true) {
    return (debug)
        ? console.log
        : () => {}
}








/** @param {Element} $ */
function cleanElement($) {
    //ToDo: Remove (style="font-style: normal;")

    $.normalize()
    if ($.hasAttribute('class') && !$.classList.length)
        $.removeAttribute('class')
    if ($.hasAttribute('style') && !$.getAttribute('style'))
        $.removeAttribute('style')
}