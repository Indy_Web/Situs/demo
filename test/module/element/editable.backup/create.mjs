// @ts-check

/// import { EditorToolbar } from './toolbar.mjs'

const enableDebug = true
const debug = enableDebug
    ? console.log
    : () => { return; }

const editableTemplate = /*html*/ 
`<style>@import '/$/style/editable.css';
:host {
    position: relative;
}
dynamic-icon {
    opacity: 0;
    width: 1.25em;
    position: absolute; top: 1em;
    &#editor-enable, &#editor-disable, &#editor-save { right: 1em; }
    &:hover   { 
        cursor: pointer;
    }
    transition: opacity 0.5s ease;
    &[visible]     { opacity: 1; }
    &:not([visible]) {
        pointer-events: none;
    }
}

</style>
<dynamic-icon id="editor-enable" src="/$/icon/pencil.svg" fill="var(--color-bg-body)" title="Edit document."></dynamic-icon>
<dynamic-icon id="editor-disable" src="/$/icon/tab-close.svg" fill="var(--color-bg-body)" title="View document."></dynamic-icon>
<dynamic-icon id="editor-save" src="/$/icon/save.svg" fill="var(--color-bg-body)" title="Save document."></dynamic-icon>
<dynamic-icon id="editor-revert" src="/$/icon/trash-x-filled.svg" fill="var(--color-bg-body)" title="Revert document."></dynamic-icon>
<slot></slot>
<editor-toolbar/>`
//

/** @param {Element} $editable */
export default function makeElementEditable($editable) {
    console.log('makeElementEditable:', $editable)

    if (!($editable instanceof Element)) return;
    const document = $editable.ownerDocument;
    const template = document.createRange()
        .createContextualFragment(editableTemplate)
    
    $editable.attachShadow({ mode: 'open' })
    $editable.shadowRoot?.replaceChildren(template.cloneNode(true))

    /** The "target" element, which is being edited:
     * @type {Element|null} */
    let $target = null;

    /**
     * The result of window.getSelection():
     * @type {Selection|null} */
    let selection = null;

    /**
     * @type {{
     *  start: Range|null,
     *  startTime: number|null,
     *  end: Range|null,
     *  hasContext: boolean
     * }}
     */
    const touch = {
        start: null,
        startTime: null,
        end: null,
        hasContext: false
    };

    let $toolbar = /** @type {HTMLElement} */
        ($editable.shadowRoot?.querySelector('editor-toolbar'));
        //? = createEditorToolbar()
    debug('$toolbar:', $toolbar)

    const $save = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-save'))
    const $enable = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-enable'))
    const $disable = /** @type {HTMLElement} */
        ($editable.shadowRoot?.getElementById('editor-disable'))
    $save.onclick = () => saveDocument($editable.innerHTML)
    $enable.onclick = () => {
        enable()
        $disable.setAttribute('visible', '')
        // $save.setAttribute('visible', '')
    }
    $disable.onclick = () => {
        disable()
        $enable.setAttribute('visible', '')
    }

    const listener = {
        showEditIcon: () => $enable.setAttribute('visible', ''),
        hideEditIcon: () => $enable.removeAttribute('visible'),
        showSaveIcon: () => $save.setAttribute('visible', ''),
        hideSaveIcon: () => $save.removeAttribute('visible'),
        showCloseIcon: () => $disable.setAttribute('visible', ''),
        hideCloseIcon: () => $disable.removeAttribute('visible'),
        handleClick: handleClick.bind($editable),
        handleContextMenu: handleContextMenu.bind($editable),
        handleMouseUp: handleMouseUp.bind($editable),
        handleTouchStart: handleTouchStart.bind($editable),
        handleTouchMove: handleTouchMove.bind($editable),
        handleTouchEnd: handleTouchEnd.bind($editable),
        handleClickOutside: handleClickOutside.bind(document),
        handleKeyDown: handleKeyDown.bind(document),
        handleKeyUp: handleKeyUp.bind(document),
    }

    /** @type {boolean} */
    let enabled = false;

    /* Object.defineProperties($editable, {
        enable: {
            value: enable,
            writable: false
        },
        disable: {
            value: disable,
            writable: false
        },
        enabled: {
            get: () => enabled
        },
        saveDocument: {
            value: saveDocument,
            writable: false
        }
    }) */

    Object.defineProperty($editable, 'editor', {
        value: Object.freeze({
            get enabled() {
                return enabled;
            },
            enable,
            disable,
            saveDocument
        }),
        writable: false
    })

    $editable.addEventListener('mouseenter', listener.showEditIcon)
    $editable.addEventListener('mouseleave', listener.hideEditIcon)

    function enable() {
        if (enabled) return;
        enabled = true;
        listener.hideEditIcon()
        $editable.setAttribute('spellcheck', 'true')

        $editable.removeEventListener('mouseenter', listener.showEditIcon)
        $editable.removeEventListener('mouseleave', listener.hideEditIcon)
        $editable.addEventListener('mouseenter', listener.showCloseIcon)
        $editable.addEventListener('mouseleave', listener.hideCloseIcon)
        // $editable.addEventListener('mouseenter', listener.showSaveIcon)
        // $editable.addEventListener('mouseleave', listener.hideSaveIcon)

        $editable.addEventListener('click', listener.handleClick)
        $editable.addEventListener('contextmenu', listener.handleContextMenu)
        $editable.addEventListener('mouseup',    listener.handleMouseUp)
        $editable.addEventListener('touchstart', listener.handleTouchStart)
        $editable.addEventListener('touchmove', listener.handleTouchMove)
        $editable.addEventListener('touchend', listener.handleTouchEnd)

    
        document.addEventListener('click',  listener.handleClickOutsid)
        document.addEventListener('keydown',listener.handleKeyDow)
        document.addEventListener('keyup',  listener.handleKeyU)
        document.addEventListener('selectionchange', () => setSelection())
    
        $editable.addEventListener('beforeinput', (e) => {
            debug('beforeinput:', e)
            // e.preventDefault()
        })
    }

    function disable() {
        if (!enabled) return;
        clearTarget()
        enabled = false;
        listener.hideCloseIcon()
        listener.hideSaveIcon()

        $editable.removeEventListener('mouseenter', listener.showCloseIcon)
        $editable.removeEventListener('mouseleave', listener.hideCloseIcon)
        // $editable.removeEventListener('mouseenter', listener.showSaveIcon)
        // $editable.removeEventListener('mouseleave', listener.hideSaveIcon)

        $editable.removeAttribute('spellcheck')
        $editable.removeEventListener('click', listener.handleClick)
        $editable.removeEventListener('contextmenu', listener.handleContextMenu)
        $editable.removeEventListener('mouseup',    listener.handleMouseUp)
    
        $editable.removeEventListener('touchstart', listener.handleTouchStart)
        $editable.removeEventListener('touchmove', listener.handleTouchMove)
        $editable.removeEventListener('touchend', listener.handleTouchEnd)
    
        document.removeEventListener('click',  listener.handleClickOutsid)
        document.removeEventListener('keydown',listener.handleKeyDow)
        document.removeEventListener('keyup',  listener.handleKeyU)
        document.removeEventListener('selectionchange', () => setSelection())
    
        $editable.addEventListener('mouseenter', listener.showEditIcon)
        $editable.addEventListener('mouseleave', listener.hideEditIcon)
    }

    /**
     * @param {HTMLElement|null} [$target] 
     * @param {{ editable: boolean|undefined }} [options]
     */
    function setTarget($target, options) {
        //! if ($target) $editable.clearTarget()
        for (const $ of document.querySelectorAll('#target')) {
            clearTarget($)
            extractEmptySpan($)
        }
        if (!$target) return;
        $target = $target
        if (options && 'editable' in options)
            $target.setAttribute('contenteditable', options.editable ? 'true' : 'false')
        window.requestAnimationFrame(() => {
            if ($target) $target.id = 'target'
            //! $target.classList.add('highlight')
        })
        $target.addEventListener('input', handleTargetInput)
        $target.focus()
    }

    /** @param {Element|null} [$] */
    function clearTarget($) {
        $target = (($) ? $ : $target) || $editable.querySelector('#target')
        if (!$target) return;
        console.log('$target:', $target)
        $target.removeEventListener('input', handleTargetInput)
        //! $target.classList.remove(...['highlight'])
        $target.removeAttribute('contenteditable')
        $target.removeAttribute('id')
        cleanElement($target)
        if ('blur' in $target)
            (/** @type {HTMLElement} */ ($target)).blur()
        $target = null
    }

    /** @param {'bold'|'italic'|'underline'|'strikethrough'} style */
    function toggleInlineStyle(style) { // bold, italic, underline, strikethrough
        if (!$target) return;
        if (style === 'italic') {
            debug('toggleInlineStyle: italic')
            if ($target.tagName === 'SPAN') {
                //* Replace with <i>:
                const $i = document.createElement('i')
                $i.innerHTML = $target.innerHTML
                $target.replaceWith($i)
            }
            else if ($target.tagName === 'I') {
                const $ = document.createElement('template')
                $.innerHTML = $target.innerHTML
                $target.after($.content)
                clearTarget()
                $target.remove()
            }
        }
        if (style === 'bold') {
            debug('toggleInlineStyle: bold')
            if ($target.tagName === 'SPAN') {
                //* Replace with <i>:
                const $b = document.createElement('b')
                $b.innerHTML = $target.innerHTML
                $target.replaceWith($b)
            }
            else if ($target.tagName === 'B') {
                const $ = document.createElement('template')
                $.innerHTML = $target.innerHTML
                $target.after($.content)
                clearTarget()
                $target.remove()
            }
        }
    }


    /** @param {MouseEvent} e  */
    function handleClick(e) {
        debug('handleClick()')
        //! if (touch.hasContext) return;

        const $clicked = /** @type {HTMLElement} */
            (document.elementFromPoint(e.clientX, e.clientY))

        if ($clicked === $editable) return;
        setTarget($clicked, { editable: Boolean(!e.ctrlKey) })
    
        if (e.ctrlKey)
            $toolbar['openMenu']($clicked)
        if (!e.ctrlKey && !$toolbar.contains($clicked))
            $toolbar['closeMenu']()

        debug('clicked:', $clicked)
    }

    /** @param {MouseEvent} e  */
    function handleContextMenu(e) {
        const p = /** @type {PointerEvent} */ (e)

        debug('contextmenu:', e)
        debug('pointerType:', p)
        debug('button:', e.button, e.buttons)

        /* //? https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
        0: Main button pressed, usually the left button or the un-initialized state
        1: Auxiliary button pressed, usually the wheel button or the middle button (if present)
        2: Secondary button pressed, usually the right button
        3: Fourth button, typically the Browser Back button
        4: Fifth button, typically the Browser Forward button
        */

        if (e.button === 2) handleRightClick(p)
        if (e.button === -1) handleTouchContextMenu(p)
    }

    /** @param {PointerEvent} e  */
    function handleRightClick(e) {
        if ($editable.classList.contains('ctrl')) return;
        const x = e.pageX
        const y = e.pageY - (document.scrollingElement?.scrollTop || 0)
        if (!selection?.isCollapsed && isWithinRange(e)) {
            e.preventDefault()

            const $span = document.createElement('span');
            (/** @type {Selection} */(selection))
                .getRangeAt(0).surroundContents($span)
            setTarget($span, { editable: true })
            selection?.removeAllRanges()
            $toolbar['openMenu']($span)
            
            // Todo consider: What if menu may open above Selection?
            // const range = (/** @type {Selection} */ (selection)).getRangeAt(0);
            // $toolbar.openMenu(range)
        }
        else {
            const $clicked = /** @type {HTMLElement} */
                (document.elementFromPoint(x, y))
            if (!$clicked || $clicked === $editable) return;
            e.preventDefault()
            setTarget($clicked, { editable: false })
            $toolbar['openMenu']($clicked)
        }
    }

    /** @param {PointerEvent} e  */
    function handleTouchContextMenu(e) {
        //ToDo: set contextMenuOpen
        //? touch.hasContext = true
    }

    /** @param {MouseEvent} e */
    function handleMouseDown(e) {
        const $target = document.elementFromPoint(e.pageX, e.pageY)
        if ($target?.id === 'target' && e.ctrlKey && e.shiftKey)
            $target?.classList.add('moving')
    }
    function handleMouseUp() {
        $target?.classList.remove('moving')
    }

    /** @param {TouchEvent} e */
    function handleTouchStart(e) {
        debug('touchstart:', e)
        const touch = e.touches[0]
        debug('touch:', touch)
        if ('caretRangeFromPoint' in document) {
            e.preventDefault()
            const x = touch.clientX
            const y = touch.clientY

            const $clicked = /** @type {HTMLElement} */
            (document.elementFromPoint(x, y))
            if ($clicked === this) return;

            if ($clicked !== this && $editable.contains($clicked) && $target !== $clicked) 
                setTarget($clicked, { editable: Boolean(!e.ctrlKey) })

            // Todo: caretRangeFromPoint is deprecated; find alternative:
            touch['start'] = document.caretRangeFromPoint(x, y)
            if (touch['start']) {
                touch['startTime'] = Date.now()
                //? Set timeout?

                setTimeout(() => {
                    selection?.removeAllRanges()
                    selection?.addRange(/** @type {Range} */
                        (touch['start']))
                }, 100)
            }
            /* if (touch['start']) {
                selection?.removeAllRanges()
                selection?.addRange(
                    (touch['start']))
            } */
        }
        else if ('caretPositionFromPoint' in document) {
            //@ts-ignore
            debug('caret:', document.caretPositionFromPoint(touch.clientX, touch.clientY))
        }
        else return;

    }

    /** @param {TouchEvent} e */
    function handleTouchMove(e) {
        const touch = e.touches[0]
        if ('caretRangeFromPoint' in document) {
            const x = touch.clientX
            const y = touch.clientY
            let range = selection?.getRangeAt(0)
            // Todo: caretRangeFromPoint is deprecated; find alternative:
            touch['end'] = document.caretRangeFromPoint(x, y)
            const { endContainer, endOffset } = touch['end'] || {}
            if (touch['start'] && touch['end'] && endContainer && typeof endOffset === 'number') {
                const comparison = touch['end'].compareBoundaryPoints(Range.START_TO_START, touch['start'])
                if (comparison === 1)
                    range?.setEnd(endContainer, endOffset)
                else if (comparison === -1)
                    range?.setStart(endContainer, endOffset)
            }
        }
        //? else if ('caretPositionFromPoint' in document) {}
        else return;
    }

    /** @param {TouchEvent} e */
    function handleTouchEnd(e) {
        debug('selection:', selection)
        debug('touchStart:', touch['start'])
        debug('touExperimenting with building a live-editing environment for documents using native browser features, egchEnd:', touch['end'])

        debug('tapped:', detectTap())

        //* Enable selection interface?
    }

    function detectTap() {
        const ellapsed = (touch['startTime'])
            ? Date.now() - touch['startTime']
            : 0
        debug('detectTap.ellapsed:', ellapsed)
        return Boolean(ellapsed < 100)
    }

    /** @param {MouseEvent} e */
    function handleClickOutside(e) {
        const $clicked = /** @type {HTMLElement} */ (e.target)
        if (!$editable.contains($clicked)) {
            clearTarget()
            if ($toolbar['isOpen'])
                $toolbar['closeMenu']()
        }
    }

    /** @param {KeyboardEvent} e */
    function handleKeyDown(e) {
        debug('keydown:', e.key)
        if (e.altKey)   $editable.classList.add('alt')
        if (e.ctrlKey)  $editable.classList.add('ctrl')
        if (e.shiftKey) $editable.classList.add('shift')
        if (!$target) return;

        //* Testing:
        if (e.key === '1') {
            e.preventDefault()

            //? https://www.w3.org/TR/input-events-1/
            //? https://developer.mozilla.org/en-US/docs/Web/API/InputEvent/InputEvent

            let i = new InputEvent('input', { inputType: 'formatItalic' })
            debug('manual input:', i)

            //! new KeyboardEvent('keydown', { key: 'i', ctrlKey: true })
        }

        if (e.key === 'Enter' && !e.shiftKey)
            insertParagraph(e)
        
        if (['Backspace', 'Delete'].includes(e.key))
            handleDeletion(e)
    
        if (['ArrowUp', 'ArrowLeft'].includes(e.key)) {
            const atStart = cursorAtTargetStart()
            debug('isAtTargetStart:', atStart)
            if (atStart)
                navigateToPreviousSibling()
        }

        if (['ArrowDown', 'ArrowRight'].includes(e.key)) {
            const atEnd = cursorAtTargetEnd()
            debug('isAtTargetEnd:', atEnd)
            if (atEnd)
                navigateToNextSibling()
        }

        // Todo fix: bug when selection.isCollapsed()
        if (e.key ==='i' && $editable.classList.contains('ctrl') && $target.tagName === 'I')
            extractInlineSelection(e)
    }

    /** @param {KeyboardEvent} e */
    function handleKeyUp(e) {
        if (!e.altKey) $editable.classList.remove('alt')
        if (!e.shiftKey) $editable.classList.remove('shift')
        if (!e.ctrlKey) {
            $editable.classList.remove('ctrl')
            //? $.setAttribute('contenteditable', 'true')
        }
        cleanElement($editable)
    }

    function cursorAtTargetStart() {
        if (!$target || !selection) return;
        if (!$target.innerHTML) return true
        return Boolean(
            $target?.firstChild && selection?.anchorNode
            && selection.anchorNode === $target?.firstChild
            && selection.anchorOffset === 0
        )
    }

    function cursorAtTargetEnd() {
        if (!$target || !selection) return;
        if (!$target.innerHTML) return true

        debug('anchorOffset:', selection.anchorOffset)
        debug('lastChild:', $target.lastChild?.textContent?.length)

        return Boolean(
            $target.lastChild && selection.anchorNode
            && selection.anchorNode === $target.lastChild
            && selection.anchorOffset === $target.lastChild.textContent?.length
        )
    }

    function selectionIncludesTargetStart() {
        if (!selection || !$target?.firstChild) return;
        return selection.getRangeAt(0)
            ?.isPointInRange($target.firstChild, 0)
    }

    function selectionIncludesTargetEnd() {
        if (!selection || !$target?.lastChild?.textContent?.length) return;
        return selection.getRangeAt(0)
            ?.isPointInRange($target.lastChild, $target.lastChild.textContent?.length)
    }

    /** @param {KeyboardEvent} [e] */
    function extractInlineSelection(e) {
        if (!$target) return;
        const includesStart = selectionIncludesTargetStart()
        const includesEnd = selectionIncludesTargetEnd()
        if (includesStart !== includesEnd) return;

        if (e) e.preventDefault()
        if (includesStart && includesEnd) {
            $target.replaceWith(...Array.from($target.childNodes))
            clearTarget()
        }
        else {
            //* Extract selection and re-insert without italics (keep other relevant formatting):
            //? '<i>A B C</i>' => '<i>A</i> B <i>C</i>'

            if (!selection) return;
            const range = selection.getRangeAt(0)
            const beforeSelection = document.createRange()
            if ($target.firstChild) {
                beforeSelection.setStart($target.firstChild, 0)
                beforeSelection.setEnd(range.startContainer, range.startOffset)
            }
            let afterSelection = document.createRange()
            if ($target.lastChild?.textContent) {
                afterSelection.setStart(range.endContainer, range.endOffset)
                afterSelection.setEnd($target.lastChild, $target.lastChild.textContent.length)
            }

            let $before = $target
            clearTarget($before)
            let $after = (/** @type {HTMLElement} */ ($before.cloneNode(true)))

            $before.append(beforeSelection.extractContents())
            $after.append(afterSelection.extractContents())
            $before.after(range.extractContents(), $after)     
        }
    }

    /** @param {PointerEvent} e */
    function isWithinRange(e) {
        let range = selection?.getRangeAt(0)
        if (!range) return;

        let area = range.getBoundingClientRect()
        let x = e.clientX;
        let y = e.clientY;

        if (x < area.left || x > area.right) return false;
        if (y < area.top  || y > area.bottom) return false;
        return true;
    }

    //ToDo: #isScrolling
    //* Modifies handleTouch functions . . compare X-Y coordinates,
    //? Or require tap to activate touch selections?

    function navigateToPreviousSibling() {
        debug('navigateToPreviousSibling()')
        const $focus = /** @type {HTMLElement} */
            ($target?.previousElementSibling)
        if (!$focus) return;
        setTarget($focus, { editable: true })
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            range.selectNodeContents($focus.lastChild || $focus)
            range.collapse(false)
            selection?.removeAllRanges()
            selection?.addRange(range)
        })
    }

    function navigateToNextSibling() {
        debug('navigateToNextSibling()')
        const $focus = /** @type {HTMLElement} */
            ($target?.nextElementSibling)
        if (!$focus) return;
        setTarget($focus, { editable: true })
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            range.selectNodeContents($focus.firstChild || $focus)
            range.collapse(true)
            selection?.removeAllRanges()
            selection?.addRange(range)
        })
    }

    /** @param {KeyboardEvent} e */
    function handleDeletion(e) {
        if (!$target || !selection) return;
        if (selection.isCollapsed) {
            if (e.key === 'Backspace') {
                if (!$target.innerHTML) {
                    e.preventDefault()
                    deleteBlock(e)
                } else if (cursorAtTargetStart())
                    combineTargetWithPreviousSibling(e)
            }
            else if (e.key === 'Delete') {
                if (!$target.innerHTML) {
                    e.preventDefault()
                    deleteBlock(e)
                } else if (cursorAtTargetEnd())
                    combineTargetWithNextSibling(e)
            }
        }
    }

    /** @param {KeyboardEvent} e */
    function insertParagraph(e) {
        e.preventDefault()
        const s = window.getSelection()
        if (!s || !$target) return;
        if (!s.isCollapsed) {
            let r = s.getRangeAt(0)
            //Todo test: r.deleteContents()
            //? r.extractContents()
            r.deleteContents()
        }

        const $p = document.createElement('p')
        if ($target.innerHTML) {
            let r = s.getRangeAt(0)
            r.setEndAfter($target?.lastChild || $target)
            $p.append(r.extractContents())
            $p.innerHTML = $p.innerHTML.trimStart()
        }
        $target.insertAdjacentElement('afterend', $p)
        setTarget($p, { editable: true })
    }

    /** @param {KeyboardEvent} e */
    function combineTargetWithPreviousSibling(e) {
        e.preventDefault()
        if (!$target) return;
        debug('combineTargetWithPreviousSibling()')
        const $from = $target
        const $to = /** @type {HTMLElement} */
            ($target.previousElementSibling)
        if (!$to) return;
        const range = document.createRange()
        range.selectNodeContents($to.lastChild || $to)
        range.collapse(false)
        const { endContainer, endOffset } = range
        //Todo: try Array.from instead:
        const n = Array.prototype.indexOf.call($to.childNodes, endContainer)
        const insertSpace = Boolean($to.textContent
            && !$to.innerHTML.endsWith('&nbsp;')
            && !$from.innerHTML.startsWith('&nbsp;')
        )
        $to.innerHTML += (insertSpace)
            ? ` ${$from.innerHTML}` : $from.innerHTML
        setTarget($to, { editable: true })
        range.setStart($to.childNodes.item(n), (
            insertSpace ? endOffset + 1 : endOffset
        ))
        range.collapse()
        selection?.removeAllRanges()
        selection?.addRange(range)
        $from.remove()
    }

    /** @param {KeyboardEvent} e */
    function combineTargetWithNextSibling(e) {
        e.preventDefault()
        if (!$target) return;
        debug('combineTargetWithNextSibling()')
        const $from = /** @type {HTMLElement} */
            ($target.nextElementSibling)
        if (!$from) return;
        const $to = $target
        const range = document.createRange()
        range.selectNodeContents($to.lastChild || $to)
        range.collapse(false)
        const { endContainer, endOffset } = range
        const n = Array.prototype.indexOf.call($to.childNodes, endContainer)
        const insertSpace = Boolean($to.textContent 
            && !$to.innerHTML.endsWith('&nbsp;')
            && !$from.innerHTML.startsWith('&nbsp;')
        )
        $to.innerHTML += (insertSpace)
            ? ` ${$from.innerHTML}` : $from.innerHTML
        //! $to.innerHTML += ` ${$from.innerHTML}`
        range.setStart($to.childNodes.item(n), endOffset)
        range.collapse()
        selection?.removeAllRanges()
        selection?.addRange(range)
        $from.remove()
    }

    /** @param {KeyboardEvent} e */
    function deleteBlock(e) {
        if (!$target) return;
        const $ = $target

        //* Select next target
        const $focus = /** @type {HTMLElement|null} */
            ((e.key === 'Backspace')
                ? $.previousElementSibling
                : (e.key === 'Delete')
                    ? $.nextElementSibling
                    : null) //? $target.parentElement || null

        //? If <li> in list, how to select aunt/uncle (sibling of parent <ul>)

        clearTarget()
        $.remove()

        if (!$focus) return;
        setTarget($focus, { editable: true })
        if (e.key === 'Delete') {
            e.preventDefault()
            e.stopPropagation()
        }
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            if (e.key === 'Backspace') {
                range.selectNodeContents($focus.lastChild || $focus)
                range.collapse(false)
            }
            if (e.key === 'Delete') {
                range.selectNodeContents($focus.firstChild || $focus)
                range.collapse(true)
            }
            selection?.removeAllRanges()
            selection?.addRange(range)
        })
    }

    /** @param {Element} $ */
    function extractEmptySpan($) {
        if ($.tagName === 'SPAN' && !$.attributes.length)
            $.replaceWith(...Array.from($.childNodes))
    }



    /** @param {Event} event */ //? InputEvent
    function handleTargetInput(event) {
        const { inputType } = /** @type {InputEvent} */ (event)
        //? debug('inputEvent', event)
        debug('inputEvent', event)
        if (inputType === 'formatItalic') event.preventDefault()
    }

    function setSelection() {
        const s = window.getSelection()
        selection = (s && s.type !== 'None') ? s : null
    }

    function cleanDocument() {
        for (const $ of $editable.getElementsByTagName('*'))
            cleanElement($)
    }
}


async function saveDocument(html) {
    // Todo: Require sanitization of innerHTML:
    //? const sanitized = DOMPurify.sanitize(html)
    //? const b = new Blob([sanitized])
    
    const b = new Blob([html])
    const body = new FormData()
    body.append('file', b, `document.${Date.now()}.html`)
    const r1 = await fetch('http://127.0.0.1:45005/api/v0/add', { method: 'POST', body })
    const j = await r1.json()
    console.dir(j)
    
    const { Hash: ipfs } = j
    
    const key = window.localStorage.getItem('document.key')
    if (!key) return console.error('Failed to retrieve content.key from localStorage.')
    else debug(`content.key:`, key)
    const r2 = await fetch(`http://127.0.0.1:45005/api/v0/name/publish?arg=${ipfs}&key=${key}&allow-offline=true`, {
        method: 'POST'
    })
    console.dir(r2)
    debug(await r2.text())

    // Todo: Saving document should pin content to IPNS key.
}









/** @param {Element} $ */
function cleanElement($) {
    //ToDo: Remove (style="font-style: normal;")

    $.normalize()
    if ($.hasAttribute('class') && !$.classList.length)
        $.removeAttribute('class')
    if ($.hasAttribute('style') && !$.getAttribute('style'))
        $.removeAttribute('style')
}

/** @param {Element|HTMLElement} $ */
function isBlockElement($) {
    const display = ('style' in $ && $.style.display) 
        || window.getComputedStyle($, '')?.display
    //? debug('elementType:', display)
    return Boolean(
        ('style' in $ && $.style.display === 'block') ||
        window.getComputedStyle($, '')?.display === 'block'
    )
}
