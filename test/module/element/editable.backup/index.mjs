// @ts-check

const enableDebug = false
const debug = enableDebug
    ? console.log
    : () => { return; }

import { EditorToolbar } from './toolbar.mjs'

export class EditableElement extends HTMLElement {
    /** The "target" element, which is being edited:
     * @type {HTMLElement|null} */
    #target;

    /**
     * The result of window.getSelection():
     * @type {Selection|null} */
    #selection;

    /**
     * @type {{
     *  start: Range|null,
     *  startTime: number|null,
     *  end: Range|null,
     *  hasContext: boolean
     * }}
     */
    #touch;

    /**
     * The <editor-toolbar> element:
     * @type {EditorToolbar} */
    #toolbar;

    //! #sanitizer;
    
    constructor() {
        super();

        const $template = document.createElement('template');
        $template.innerHTML = /* html */ `
        <!-- 
            <style>
                @import '../../src/styles/editor-toolbar.scss';
            </style>
        -->
            <style>
                @import '/css/editor-toolbar.css';
            </style>
            <menu>
                <button id='save'>Save</button>
                <!--<dynamic-icon src="/icons/telegram.svg" />-->
            </menu>
            <slot></slot>
            <editor-toolbar/>
        `

        this.attachShadow({ mode: 'open' })
        this.shadowRoot?.appendChild($template.content.cloneNode(true))
        
        this.#target = null
        this.#selection = null
        this.#touch = {
            start: null,
            startTime: null,
            end: null,
            hasContext: false
        }
        this.#toolbar = /** @type {EditorToolbar} */
            (this.shadowRoot?.querySelector('editor-toolbar'))

        //ToDo: Extract controls from custom element to allow customization.
        const $save = /** @type {HTMLElement} */
            (this.shadowRoot?.getElementById('save'))
        $save.onclick = this.saveDocument

        this.addEventListener('beforeinput', (e) => {
            debug('beforeinput:', e)
            // e.preventDefault()
        })

        this.setAttribute('spellcheck', 'true')
        this.addEventListener('click',      (e) => this.#handleClick(e))
        this.addEventListener('contextmenu', (e) => this.#handleContextMenu(e))
        this.addEventListener('mousedown',  (e) => this.#handleMouseDown(e))
        this.addEventListener('mouseup',    (e) => this.#handleMouseUp())
        this.addEventListener('touchstart', (e) => this.#handleTouchStart(e))
        this.addEventListener('touchmove', (e) => this.#handleTouchMove(e))
        this.addEventListener('touchend', (e) => this.#handleTouchEnd(e))

        document.addEventListener('click',  (e) => this.#handleClickOutside(e))
        document.addEventListener('keydown',(e) => this.#handleKeyDown(e))
        document.addEventListener('keyup',  (e) => this.#handleKeyUp(e))
        document.addEventListener('selectionchange', () => this.#setSelection())
    }


    /** 
     * @param {string} html 
     * @param {boolean} [sanitize]
    */
    loadDocument(html, sanitize=false) {
        if (!html) return;
        const supported = Boolean('setHTML' in this)

        //? if ('setHTML' in this) this.setHTML(html)
        /// else this.innerHTML = html //? DOMpurify
        // ToDo: Allow sanitized <custom-elements>, which are registered.
        //! this.innerHTML = html

        //! 'allowElements' option is not functioning.
        // @ts-ignore
        if (sanitize && supported) this.setHTML(html, new Sanitizer({
            allowElements: ['github-color-icon', 'telegram-color-icon', 'handshake-icon']
        }))  //? 'telegram . . handshake . . dynamic-icon . . 
        else this.innerHTML = html
        if (!sanitize || !supported) console.warn('No sanitization of html content.')
    }


    async saveDocument() {
        // Todo: Require sanitization of innerHTML:
        //? const sanitized = DOMPurify.sanitize(this.innerHTML)
        //? const b = new Blob([sanitized])
        
        const b = new Blob([this.innerHTML])
        const body = new FormData()
        body.append('file', b, `document.${Date.now()}.html`)
        const r1 = await fetch('http://127.0.0.1:45005/api/v0/add', { method: 'POST', body })
        const j = await r1.json()
        console.dir(j)
        
        const { Hash: ipfs } = j
        
        const key = window.localStorage.getItem('document.key')
        if (!key) return console.error('Failed to retrieve content.key from localStorage.')
        else debug(`content.key:`, key)
        const r2 = await fetch(`http://127.0.0.1:45005/api/v0/name/publish?arg=${ipfs}&key=${key}&allow-offline=true`, {
            method: 'POST'
        })
        console.dir(r2)
        debug(await r2.text())

        // Todo: Saving document should pin content to IPNS key.
    }

    /**
     * @param {HTMLElement|null} [$target] 
     * @param {{ editable: boolean|undefined }} [options]
     */
    setTarget($target, options) {
        //! if (this.#target) this.clearTarget()
        for (const $ of document.querySelectorAll('#target')) {
            this.clearTarget($)
            this.#extractEmptySpan($)
        }
        if (!$target) return;
        this.#target = $target
        if (options && 'editable' in options)
            $target.setAttribute('contenteditable', options.editable ? 'true' : 'false')
        window.requestAnimationFrame(() => {
            $target.id = 'target'
            //! $target.classList.add('highlight')
        })
        $target.addEventListener('input', this.#handleTargetInput)
        $target.focus()
    }

    /**
     * @param {Element|null} [$] 
     */
    clearTarget($) {
        let $target = ($) ? $ : this.#target
        if (!$target) return;
        $target.removeEventListener('input', this.#handleTargetInput)
        //! $target.classList.remove(...['highlight'])
        $target.removeAttribute('contenteditable')
        $target.removeAttribute('id')
        cleanElement($target)
        if ('blur' in $target)
            (/** @type {HTMLElement} */ ($target)).blur()
        $target = null
    }

    /** @param {'bold'|'italic'|'underline'|'strikethrough'} style */
    toggleInlineStyle(style) { // bold, italic, underline, strikethrough
        if (!this.#target) return;
        if (style === 'italic') {
            debug('toggleInlineStyle: italic')
            if (this.#target.tagName === 'SPAN') {
                //* Replace with <i>:
                const $i = document.createElement('i')
                $i.innerHTML = this.#target.innerHTML
                this.#target.replaceWith($i)
            }
            else if (this.#target.tagName === 'I') {
                const $ = document.createElement('template')
                $.innerHTML = this.#target.innerHTML
                let $target = this.#target
                $target.after($.content)
                this.clearTarget()
                $target.remove()
            }
        }
        if (style === 'bold') {
            debug('toggleInlineStyle: bold')
            if (this.#target.tagName === 'SPAN') {
                //* Replace with <i>:
                const $b = document.createElement('b')
                $b.innerHTML = this.#target.innerHTML
                this.#target.replaceWith($b)
            }
            else if (this.#target.tagName === 'B') {
                const $ = document.createElement('template')
                $.innerHTML = this.#target.innerHTML
                let $target = this.#target
                $target.after($.content)
                this.clearTarget()
                $target.remove()
            }
        }
    }

    /** @param {MouseEvent} e  */
    #handleClick(e) {
        debug('handleClick()')
        //! if (this.#touch.hasContext) return;

        const $clicked = /** @type {HTMLElement} */
            (document.elementFromPoint(e.clientX, e.clientY))

        if ($clicked === this) return;
        this.setTarget($clicked, { editable: Boolean(!e.ctrlKey) })
  
        if (e.ctrlKey)
            this.#toolbar.openMenu($clicked)
        if (!e.ctrlKey && !this.#toolbar.contains($clicked))
            this.#toolbar.closeMenu()

        debug('clicked:', $clicked)
    }


    /** @param {MouseEvent} e  */
    #handleContextMenu(e) {
        const p = /** @type {PointerEvent} */ (e)

        debug('contextmenu:', e)
        debug('pointerType:', p)
        debug('button:', e.button, e.buttons)

        /* //? https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
        0: Main button pressed, usually the left button or the un-initialized state
        1: Auxiliary button pressed, usually the wheel button or the middle button (if present)
        2: Secondary button pressed, usually the right button
        3: Fourth button, typically the Browser Back button
        4: Fifth button, typically the Browser Forward button
        */

        if (e.button === 2) this.#handleRightClick(p)
        if (e.button === -1) this.#handleTouchContextMenu(p)
    }


    /** @param {PointerEvent} e  */
    #handleRightClick(e) {
        if (this.classList.contains('ctrl')) return;
        const x = e.pageX
        const y = e.pageY - (document.scrollingElement?.scrollTop || 0)
        if (!this.#selection?.isCollapsed && this.#isWithinRange(e)) {
            e.preventDefault()

            const $span = document.createElement('span');
            (/** @type {Selection} */(this.#selection))
                .getRangeAt(0).surroundContents($span)
            this.setTarget($span, { editable: true })
            this.#selection?.removeAllRanges()
            this.#toolbar.openMenu($span)
            
            // Todo consider: What if menu may open above Selection?
            // const range = (/** @type {Selection} */ (this.#selection)).getRangeAt(0);
            // this.#toolbar.openMenu(range)
        }
        else {
            const $clicked = /** @type {HTMLElement} */
                (document.elementFromPoint(x, y))
            if (!$clicked || $clicked === this) return;
            e.preventDefault()
            this.setTarget($clicked, { editable: false })
            this.#toolbar.openMenu($clicked)
        }
    }

    /** @param {PointerEvent} e  */
    #handleTouchContextMenu(e) {
        //ToDo: set contextMenuOpen
        //? this.#touch.hasContext = true
    }

    /** @param {MouseEvent} e */
    #handleMouseDown(e) {
        const $target = document.elementFromPoint(e.pageX, e.pageY)
        if ($target?.id === 'target' && e.ctrlKey && e.shiftKey)
            this.#target?.classList.add('moving')
    }
    #handleMouseUp() {
        this.#target?.classList.remove('moving')
    }

    /** @param {TouchEvent} e */
    #handleTouchStart(e) {
        debug('touchstart:', e)
        const touch = e.touches[0]
        debug('touch:', touch)
        if ('caretRangeFromPoint' in document) {
            e.preventDefault()
            const x = touch.clientX
            const y = touch.clientY

            const $clicked = /** @type {HTMLElement} */
            (document.elementFromPoint(x, y))
            if ($clicked === this) return;

            if ($clicked !== this && this.contains($clicked) && this.#target !== $clicked) 
                this.setTarget($clicked, { editable: Boolean(!e.ctrlKey) })

            // Todo: caretRangeFromPoint is deprecated; find alternative:
            this.#touch.start = document.caretRangeFromPoint(x, y)
            if (this.#touch.start) {
                this.#touch.startTime = Date.now()
                //? Set timeout?

                setTimeout(() => {
                    this.#selection?.removeAllRanges()
                    this.#selection?.addRange(/** @type {Range} */
                        (this.#touch.start))
                }, 100)
            }
            /* if (this.#touch.start) {
                this.#selection?.removeAllRanges()
                this.#selection?.addRange(
                    (this.#touch.start))
            } */
        }
        else if ('caretPositionFromPoint' in document) {
            //@ts-ignore
            debug('caret:', document.caretPositionFromPoint(touch.clientX, touch.clientY))
        }
        else return;

    }

    /** @param {TouchEvent} e */
    #handleTouchMove(e) {
        const touch = e.touches[0]
        if ('caretRangeFromPoint' in document) {
            const x = touch.clientX
            const y = touch.clientY
            let range = this.#selection?.getRangeAt(0)
            // Todo: caretRangeFromPoint is deprecated; find alternative:
            this.#touch.end = document.caretRangeFromPoint(x, y)
            const { endContainer, endOffset } = this.#touch.end || {}
            if (this.#touch.start && this.#touch.end && endContainer && typeof endOffset === 'number') {
                const comparison = this.#touch.end.compareBoundaryPoints(Range.START_TO_START, this.#touch.start)
                if (comparison === 1)
                    range?.setEnd(endContainer, endOffset)
                else if (comparison === -1)
                    range?.setStart(endContainer, endOffset)
            }
        }
        //? else if ('caretPositionFromPoint' in document) {}
        else return;
    }

    /** @param {TouchEvent} e */
    #handleTouchEnd(e) {
        debug('selection:', this.#selection)
        debug('touchStart:', this.#touch.start)
        debug('touExperimenting with building a live-editing environment for documents using native browser features, egchEnd:', this.#touch.end)

        debug('tapped:', this.#detectTap())

        //* Enable selection interface?
    }

    #detectTap() {
        const ellapsed = (this.#touch.startTime)
            ? Date.now() - this.#touch.startTime
            : 0
        debug('detectTap.ellapsed:', ellapsed)
        return Boolean(ellapsed < 100)
    }

    /** @param {MouseEvent} e */
    #handleClickOutside(e) {
        const $clicked = /** @type {HTMLElement} */ (e.target)
        if (!this.contains($clicked)) {
            this.clearTarget()
            if (this.#toolbar.isOpen)
                this.#toolbar.closeMenu()
        }
    }

    /** @param {KeyboardEvent} e */
    #handleKeyDown(e) {
        debug('keydown:', e.key)
        if (e.altKey)   this.classList.add('alt')
        if (e.ctrlKey)  this.classList.add('ctrl')
        if (e.shiftKey) this.classList.add('shift')
        if (!this.#target) return;

        //* Testing:
        if (e.key === '1') {
            e.preventDefault()

            //? https://www.w3.org/TR/input-events-1/
            //? https://developer.mozilla.org/en-US/docs/Web/API/InputEvent/InputEvent

            let i = new InputEvent('input', { inputType: 'formatItalic' })
            debug('manual input:', i)

            //! new KeyboardEvent('keydown', { key: 'i', ctrlKey: true })
        }

        if (e.key === 'Enter' && !e.shiftKey)
            this.#insertParagraph(e)
        
        if (['Backspace', 'Delete'].includes(e.key))
            this.#handleDeletion(e)
  
        if (['ArrowUp', 'ArrowLeft'].includes(e.key)) {
            const atStart = this.#cursorAtTargetStart()
            debug('isAtTargetStart:', atStart)
            if (atStart)
                this.#navigateToPreviousSibling()
        }

        if (['ArrowDown', 'ArrowRight'].includes(e.key)) {
            const atEnd = this.#cursorAtTargetEnd()
            debug('isAtTargetEnd:', atEnd)
            if (atEnd)
                this.#navigateToNextSibling()
        }

        // Todo fix: bug when selection.isCollapsed()
        if (e.key ==='i' && this.classList.contains('ctrl') && this.#target.tagName === 'I')
            this.#extractInlineSelection(e)
    }

    /** @param {KeyboardEvent} e */
    #handleKeyUp(e) {
        if (!e.altKey) this.classList.remove('alt')
        if (!e.shiftKey) this.classList.remove('shift')
        if (!e.ctrlKey) {
            this.classList.remove('ctrl')
            //? $.setAttribute('contenteditable', 'true')
        }
        cleanElement(this)
    }

    #cursorAtTargetStart() {
        if (!this.#target || !this.#selection) return;
        if (!this.#target.innerHTML) return true
        return Boolean(
            this.#target?.firstChild && this.#selection?.anchorNode
            && this.#selection.anchorNode === this.#target?.firstChild
            && this.#selection.anchorOffset === 0
        )
    }

    #cursorAtTargetEnd() {
        if (!this.#target || !this.#selection) return;
        if (!this.#target.innerHTML) return true

        debug('anchorOffset:', this.#selection.anchorOffset)
        debug('lastChild:', this.#target.lastChild?.textContent?.length)

        return Boolean(
            this.#target.lastChild && this.#selection.anchorNode
            && this.#selection.anchorNode === this.#target.lastChild
            && this.#selection.anchorOffset === this.#target.lastChild.textContent?.length
        )
    }

    #selectionIncludesTargetStart() {
        if (!this.#selection || !this.#target?.firstChild) return;
        return this.#selection.getRangeAt(0)
            ?.isPointInRange(this.#target.firstChild, 0)
    }

    #selectionIncludesTargetEnd() {
        if (!this.#selection || !this.#target?.lastChild?.textContent?.length) return;
        return this.#selection.getRangeAt(0)
            ?.isPointInRange(this.#target.lastChild, this.#target.lastChild.textContent?.length)
    }

    /** @param {KeyboardEvent} [e] */
    #extractInlineSelection(e) {
        if (!this.#target) return;
        const includesStart = this.#selectionIncludesTargetStart()
        const includesEnd = this.#selectionIncludesTargetEnd()
        if (includesStart !== includesEnd) return;

        if (e) e.preventDefault()
        if (includesStart && includesEnd) {
            this.#target.replaceWith(...Array.from(this.#target.childNodes))
            this.clearTarget()
        }
        else {
            //* Extract selection and re-insert without italics (keep other relevant formatting):
            //? '<i>A B C</i>' => '<i>A</i> B <i>C</i>'

            if (!this.#selection) return;
            let selection = this.#selection.getRangeAt(0)
            let beforeSelection = document.createRange()
            if (this.#target.firstChild) {
                beforeSelection.setStart(this.#target.firstChild, 0)
                beforeSelection.setEnd(selection.startContainer, selection.startOffset)
            }
            let afterSelection = document.createRange()
            if (this.#target.lastChild?.textContent) {
                afterSelection.setStart(selection.endContainer, selection.endOffset)
                afterSelection.setEnd(this.#target.lastChild, this.#target.lastChild.textContent.length)
            }

            let $before = this.#target
            this.clearTarget($before)
            let $after = (/** @type {HTMLElement} */ ($before.cloneNode(true)))

            $before.append(beforeSelection.extractContents())
            $after.append(afterSelection.extractContents())
            $before.after(selection.extractContents(), $after)     
        }
    }

    /** @param {PointerEvent} e */
    #isWithinRange(e) {
        let range = this.#selection?.getRangeAt(0)
        if (!range) return;

        let area = range.getBoundingClientRect()
        let x = e.clientX;
        let y = e.clientY;

        if (x < area.left || x > area.right) return false;
        if (y < area.top  || y > area.bottom) return false;
        return true;
    }

    //ToDo: #isScrolling
    //* Modifies handleTouch functions . . compare X-Y coordinates,
    //? Or require tap to activate touch selections?

    #navigateToPreviousSibling() {
        debug('navigateToPreviousSibling()')
        const $focus = /** @type {HTMLElement} */
            (this.#target?.previousElementSibling)
        if (!$focus) return;
        this.setTarget($focus, { editable: true })
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            range.selectNodeContents($focus.lastChild || $focus)
            range.collapse(false)
            this.#selection?.removeAllRanges()
            this.#selection?.addRange(range)
        })
    }

    #navigateToNextSibling() {
        debug('navigateToNextSibling()')
        const $focus = /** @type {HTMLElement} */
            (this.#target?.nextElementSibling)
        if (!$focus) return;
        this.setTarget($focus, { editable: true })
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            range.selectNodeContents($focus.firstChild || $focus)
            range.collapse(true)
            this.#selection?.removeAllRanges()
            this.#selection?.addRange(range)
        })
    }

    /** @param {KeyboardEvent} e */
    #handleDeletion(e) {
        if (!this.#target || !this.#selection) return;
        if (this.#selection.isCollapsed) {
            if (e.key === 'Backspace') {
                if (!this.#target.innerHTML) {
                    e.preventDefault()
                    this.#deleteBlock(e)
                } else if (this.#cursorAtTargetStart())
                    this.#combineTargetWithPreviousSibling(e)
            }
            else if (e.key === 'Delete') {
                if (!this.#target.innerHTML) {
                    e.preventDefault()
                    this.#deleteBlock(e)
                } else if (this.#cursorAtTargetEnd())
                    this.#combineTargetWithNextSibling(e)
            }
        }
    }

    /** @param {KeyboardEvent} e */
    #insertParagraph(e) {
        e.preventDefault()
        const s = window.getSelection()
        if (!s || !this.#target) return;
        if (!s.isCollapsed) {
            let r = s.getRangeAt(0)
            //Todo test: r.deleteContents()
            //? r.extractContents()
            r.deleteContents()
        }

        const $p = document.createElement('p')
        if (this.#target.innerHTML) {
            let r = s.getRangeAt(0)
            r.setEndAfter(this.#target?.lastChild || this.#target)
            $p.append(r.extractContents())
            $p.innerHTML = $p.innerHTML.trimStart()
        }
        this.#target.insertAdjacentElement('afterend', $p)
        this.setTarget($p, { editable: true })
    }

    /** @param {KeyboardEvent} e */
    #combineTargetWithPreviousSibling(e) {
        e.preventDefault()
        if (!this.#target) return;
        debug('combineTargetWithPreviousSibling()')
        const $from = this.#target
        const $to = /** @type {HTMLElement} */
            (this.#target.previousElementSibling)
        if (!$to) return;
        const range = document.createRange()
        range.selectNodeContents($to.lastChild || $to)
        range.collapse(false)
        const { endContainer, endOffset } = range
        //Todo: try Array.from instead:
        const n = Array.prototype.indexOf.call($to.childNodes, endContainer)
        const insertSpace = Boolean($to.textContent
            && !$to.innerHTML.endsWith('&nbsp;')
            && !$from.innerHTML.startsWith('&nbsp;')
        )
        $to.innerHTML += (insertSpace)
            ? ` ${$from.innerHTML}` : $from.innerHTML
        this.setTarget($to, { editable: true })
        range.setStart($to.childNodes.item(n), (
            insertSpace ? endOffset + 1 : endOffset
        ))
        range.collapse()
        this.#selection?.removeAllRanges()
        this.#selection?.addRange(range)
        $from.remove()
    }

    /** @param {KeyboardEvent} e */
    #combineTargetWithNextSibling(e) {
        e.preventDefault()
        if (!this.#target) return;
        debug('combineTargetWithNextSibling()')
        const $from = /** @type {HTMLElement} */
            (this.#target.nextElementSibling)
        if (!$from) return;
        const $to = this.#target
        const range = document.createRange()
        range.selectNodeContents($to.lastChild || $to)
        range.collapse(false)
        const { endContainer, endOffset } = range
        const n = Array.prototype.indexOf.call($to.childNodes, endContainer)
        const insertSpace = Boolean($to.textContent 
            && !$to.innerHTML.endsWith('&nbsp;')
            && !$from.innerHTML.startsWith('&nbsp;')
        )
        $to.innerHTML += (insertSpace)
            ? ` ${$from.innerHTML}` : $from.innerHTML
        //! $to.innerHTML += ` ${$from.innerHTML}`
        range.setStart($to.childNodes.item(n), endOffset)
        range.collapse()
        this.#selection?.removeAllRanges()
        this.#selection?.addRange(range)
        $from.remove()
    }

    /** @param {KeyboardEvent} e */
    #deleteBlock(e) {
        if (!this.#target) return;
        const $ = this.#target

        //* Select next target
        const $focus = /** @type {HTMLElement|null} */
            ((e.key === 'Backspace')
                ? $.previousElementSibling
                : (e.key === 'Delete')
                    ? $.nextElementSibling
                    : null) //? $target.parentElement || null

        //? If <li> in list, how to select aunt/uncle (sibling of parent <ul>)

        this.clearTarget()
        $.remove()

        if (!$focus) return;
        this.setTarget($focus, { editable: true })
        if (e.key === 'Delete') {
            e.preventDefault()
            e.stopPropagation()
        }
        window.requestAnimationFrame(() => {
            const range = document.createRange()
            if (e.key === 'Backspace') {
                range.selectNodeContents($focus.lastChild || $focus)
                range.collapse(false)
            }
            if (e.key === 'Delete') {
                range.selectNodeContents($focus.firstChild || $focus)
                range.collapse(true)
            }
            this.#selection?.removeAllRanges()
            this.#selection?.addRange(range)
        })
    }

    /** @param {Element} $ */
    #extractEmptySpan($) {
        if ($.tagName === 'SPAN' && !$.attributes.length)
            $.replaceWith(...Array.from($.childNodes))
    }



    /** @param {Event} event */ //? InputEvent
    #handleTargetInput(event) {
        const { inputType } = /** @type {InputEvent} */ (event)
        //? debug('inputEvent', event)
        debug('inputEvent', event)
        if (inputType === 'formatItalic') event.preventDefault()
    }

    #setSelection() {
        const s = window.getSelection()
        this.#selection = (s && s.type !== 'None') ? s : null
    }

    #cleanDocument() {
        for (const $ of this.getElementsByTagName('*'))
            cleanElement($)
    }
}


//? customElements.define('editable-article', EditableElement)

customElements.define('editable-article', 
class EditableArticle extends EditableElement {
    constructor() {
        super();
    }
})

/*
customElements.define('editable-header',
class EditableHeader extends EditableElement {
    constructor() {
        super();
    }
})

customElements.define('editable-footer',
class EditableFooter extends EditableElement {
    constructor() {
        super();
    }
})*/



/** @param {Element} $ */
function cleanElement($) {
    //ToDo: Remove (style="font-style: normal;")

    $.normalize()
    if ($.hasAttribute('class') && !$.classList.length)
        $.removeAttribute('class')
    if ($.hasAttribute('style') && !$.getAttribute('style'))
        $.removeAttribute('style')
}

/** @param {Element|HTMLElement} $ */
function isBlockElement($) {
    const display = ('style' in $ && $.style.display) 
        || window.getComputedStyle($, '')?.display
    //? debug('elementType:', display)
    return Boolean(
        ('style' in $ && $.style.display === 'block') ||
        window.getComputedStyle($, '')?.display === 'block'
    )
}


// ToDo: abstract sanitizer function:
/* 
function sanitizeArticle() {
    const sanitizer = new Sanitizer({

    })
} */


// Todo fix:
/*
    * 'ctrl' class is not always cleared,
        having to do with leaving window focus while CTRL pressed,
        and window not detecting the keyUp event to clear class.
        Fix: when window regains focus, check state of CTRL and update DOM
*/


// Todo improvements:
/*
    * Disable clicking on links while editing article.
*/