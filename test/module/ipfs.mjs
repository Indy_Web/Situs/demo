import config from '../config.mjs';
export const rpc = await RPC()
export default { rpc }

async function RPC() {
    const host = config.ipfs?.rpc
    if (
        !(host && typeof host === 'string') ||
        !(await isConnected())
    )   return null;
    
    return Object.freeze({
        /** 
         * @param {FormData} body
         * @param {Partial<{toFiles:string}>} [options] */
        async add(body, options) {
            const endpoint = `${host}/add`
                + ((options?.toFiles) ? `?to-files=${options.toFiles}` : '')
            const response = await fetch(endpoint, {
                method: 'POST',
                body
            })
            return await response.json()
        },

        /**
         * @typedef {Object} addTextFileOptions
         * @property {string} type
         * @property {string} name
         * @property {string} path
         * @param {string} text
         * @param {Partial<addTextFileOptions>} [options] */
        async addText(text='', options) {
            const type = options?.type || 'text/plain'
            const name = options?.name || ''
            const blob = new Blob([text], { type })
            const body = new FormData()
            body.append('file', blob, name)

            return await this.add(body, {
                toFiles: options?.path
            })
        },

        /**
         * @typedef {Object} addFileOptions
         * @property {string} type
         * @property {string} name
         * property {string} path
         * @param {string|Blob} content
         * @param {Partial<addTextFileOptions>} [options] */
        async addFile(content, options) {
            const type = (content instanceof Blob)
                ? content?.type
                : options?.type || 'text/plain'
            const name = options?.name || ''
            const blob = (content instanceof Blob)
                ? content
                : (typeof content === 'string')
                    ? new Blob([content], { type })
                    : undefined
            if (!blob) return;
            const body = new FormData()
            body.append('file', blob, name)
            return await this.add(body)
            /* , {
                toFiles: options?.path
            }) */
        },

        /**
         * @param {string} path
         * @param {Partial<{offset:number,length:number}>} [options] */
        async cat(path, options) {
            const endpoint = host
                + '/cat'
                + `?arg=${path}`
                + ((options?.offset) ? `&offset=${options.offset}` : '')
                + ((options?.length) ? `&length=${options.length}` : '')
            return await getText(endpoint)
        },

        files: {
            /**
             * @param {string} source
             * @param {string} dest
             * @param {Partial<{ parents: boolean }>} [options] */
            async cp(source, dest, options) {
                const endpoint = host
                    + '/files/cp'
                    + `?arg=${source}&arg=${dest}`
                    + ((options?.parents) ? '&parents=true' : '')
                return await isOkay(endpoint)
            },

            /** @param {string} path */
            async ls(path) {
                const endpoint = host
                    + '/files/ls'
                    + `?arg=${path}`
                const entries = /** @type {string[]} */(
                    (await getJson(endpoint)).Entries //@ts-ignore:
                        ?.map(entry => entry.Name) || []
                )
                return entries
            },

            /*
            /** @param {string} path * /
            async type(path) {
                const { Hash: cid, Type: type } = await this.stat(path)
                if (type === 'directory') return type;
                if (tpye !== 'file') return null
                const response = await fetch(`http://127.0.0.1:48084/ipfs/${cid}`, { method: 'HEAD' })
                return response.headers.get('content-type')?.split(';').at(0) || null
            }, */

            /**
             * @param {string} path
             * @param {Partial<{ parents: boolean }>} [options] */
            async mkdir(path, options) {
                const endpoint = host
                    + '/files/mkdir'
                    + `?arg=${path}`
                    + ((options?.parents) ? '&parents=true' : '')
                return await isOkay(endpoint)
            },

            /**
             * @param {string} path 
             * @param {Partial<{offset:number, count:number}>} [options] */
            async read(path, options) {
                const endpoint = host
                    + '/files/read'
                    + `?arg=${path}`
                    + ((options?.offset) ? `&offset=${options.offset}` : '')
                    + ((options?.count) ? `&count=${options.count}` : '')
                return await getText(endpoint)
            },

            /**
             * @param {string} path
             * @param {Partial<{ recursive: boolean, force: boolean }>} [options] */
            async rm(path, options) {
                const endpoint = host
                    + '/files/rm'
                    + `?arg=${path}`
                    + ((options?.recursive) ? '&recursive=true' : '')
                    + ((options?.force) ? '&force=true' : '')
                return await isOkay(endpoint)
            },

            /** @param {string} path */
            async stat(path) {
                const endpoint = host + '/files/stat' + `?arg=${path}`
                return await getJson(endpoint)
            },

            /**
             * @param {string} cid
             * @param {string} path */
            async write(cid, path) {
                if (!(cid && path)) return;
                const stat = await this.stat(path);
                if (cid === stat?.Hash) return;
                console.log('write', cid, path)
                if (stat) await this.rm(path, {
                    recursive: true
                })
                await this.cp(`/ipfs/${cid}`, path, {
                    parents: true
                })
            }
        },

        async id() {
            const endpoint = host + '/id'
            return await getJson(endpoint)
        },

        key: {
            /** @param {Partial<{ l: boolean }>} [options] */
            async list(options) {
                const endpoint = host
                    + '/key/list'
                    + ((options?.l) ? '?l=true' : '')
                return await getJson(endpoint)
            },

            /** @param {string} name */
            async has(name) {
                try {
                    const { Keys: keys } = await this.list({ l: true }) //@ts-ignore:
                    return Boolean(keys?.find(key => key.Id === name))
                }
                catch {}
                return false
            }
        },

        name: {
            /**
             * @param {string} name 
             * @param {Partial<{recursive: boolean}>} [options] */
            async resolve(name, options) {
                const endpoint = host
                    + '/name/resolve'
                    + `?arg=${name}`
                    + ((options?.recursive === false)
                        ? '&recursive=false' : '')
                /** @type {Partial<{ipfs:string, ipns:string}>} */
                let address = {
                    ipfs: '', ipns: ''
                }
                try {
                    const path = (await getJson(endpoint))?.Path
                    const [key, protocol] = path.match(/(?<=\/(ipfs|ipns)\/)[\w]+$/) || []
                    if (protocol in address) //@ts-ignore:
                        address[protocol] = key
                } catch {}
                return address
            },

            /** @param {string} name */
            async resolveAll(name) {
                let address = { ipfs: '', ipns: '' }
                try {
                    while (name) {
                        const { ipfs='', ipns='' } = await this.resolve(name, {
                            recursive: false
                        })
                        if (ipfs) Object.assign(address, { ipfs })
                        if (ipns) Object.assign(address, { ipns })
                        name = ipns || ''
                    }
                } catch {}
                return address
            },

            /**
             * @param {string} ipfs 
             * @param {string} ipns */
            async publish(ipfs, ipns) {
                const endpoint = host
                    + '/name/publish'
                    + `?arg=${ipfs}`
                    + `&key=${ipns}`
                    + '&allow-offline=true'
                return await getJson(endpoint)
            }
        }
    })

    async function isConnected() {
        try {
            const endpoint = host + '/id'
            const json = await getJson(endpoint)
            return Boolean(json?.ID)
        }
        catch {}
        return false
    }

    /** @param {string} endpoint */
    function post(endpoint) {
        return fetch(endpoint, {
            method: 'POST'
        })
    }

    /** @param {string} endpoint */
    async function getJson(endpoint) {
        try {
            const response = await post(endpoint)
            if (response.ok)
                return await response.json()
        }
        catch {}
        return null
    }

    /** @param {string} endpoint */
    async function getText(endpoint) {
        try {
            const response = await post(endpoint)
            if (response.ok)
                return await response.text()
        }
        catch {}
        return null
    }

    /** @param {string} endpoint */
    async function isOkay(endpoint) {
        try {
            const response = await post(endpoint)
            return response.ok
        }
        catch {}
        return false
    }
}