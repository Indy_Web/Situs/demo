import * as UFO from '../lib/ufo@1.3.2/index.mjs';
import * as Hypertext from './hypertext.mjs'
console.log(`Hypertext v${Hypertext.version}`)

import { site } from './site.mjs';
import { rpc } from './ipfs.mjs';

Object.defineProperties(window, {
    UFO: { writable: false, value: UFO },
    Situs: { value: Object.freeze({
        site,
        ipfs: rpc
    }) },
    startViewTransition: {
        value: startViewTransition,
        writable: false
    }
})

export default {
    site,
    ipfs: { rpc },
    loadDocument,
    useNavigation
}

/** @type {Hypertext.HypertextElement} */
const $root = document.documentElement
export async function loadDocument() {
    await Hypertext.run($root)
    await startViewTransition($root.render)
}

export function useNavigation() {
    if ('navigation' in window) // @ts-ignore
        navigation.addEventListener('navigate', (event) =>
            handleNavigation(event))
}

/** @param {Event|any} e */
function handleNavigation(e) {
    console.log('NavigationEvent:', e)
    const url = new UFO.$URL(e.destination.url)
    if (url.host !== location.host) return;
    //? do not intercept non-html/index resources?

    const { navigationType: type } = e //? 'push', 'reload', 'replace', 'traverse'
    console.log(`navigation (${type}) to:`, url)

    e.intercept({
        async handler() {
            const context = await Hypertext.get(url.href)
            Hypertext.integrate(context?.documentElement, $root)
        }
    })


    function navigateContexts() {
        //* Read next context . . integrate with current document.
    }
}

/** 
 * @param {Function|any} updateDOM 
 * @returns {Promise<any>} */
function startViewTransition(updateDOM) {
    if (!(updateDOM instanceof Function)) {
        console.warn("Provide a Function that updates the DOM to 'startViewTransition'.")
        return Promise.resolve();
    }
    return ('startViewTransition' in document) // @ts-ignore:
        ? document.startViewTransition(updateDOM).finished
        : Promise.resolve(updateDOM())
}